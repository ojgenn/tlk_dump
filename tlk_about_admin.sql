-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: dev-db.talkabout.site    Database: talkabout_db_admin
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_usr_activityObjects`
--

DROP TABLE IF EXISTS `tbl_usr_activityObjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_activityObjects` (
  `objectTypeID` int(11) NOT NULL,
  `objectTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`objectTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_activityTypes`
--

DROP TABLE IF EXISTS `tbl_usr_activityTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_activityTypes` (
  `activityTypeID` int(11) NOT NULL,
  `activityTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`activityTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_userTokens`
--

DROP TABLE IF EXISTS `tbl_usr_userTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_userTokens` (
  `userID` bigint(20) NOT NULL,
  `userToken` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_users`
--

DROP TABLE IF EXISTS `tbl_usr_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_users` (
  `userID` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8 NOT NULL,
  `userPassword` varchar(50) CHARACTER SET utf8 NOT NULL,
  `statusTypeID` int(11) NOT NULL DEFAULT '1',
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_usersActivity`
--

DROP TABLE IF EXISTS `tbl_usr_usersActivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_usersActivity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `activityTypeID` int(11) NOT NULL,
  `freeText` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `objectTypeID` int(11) NOT NULL DEFAULT '0',
  `objectID` bigint(20) DEFAULT '0',
  `activityTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5137 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'talkabout_db_admin'
--

--
-- Dumping routines for database 'talkabout_db_admin'
--
/*!50003 DROP FUNCTION IF EXISTS `fn_usr_validateUserToken` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` FUNCTION `fn_usr_validateUserToken`(_userID bigint, _token nvarchar(100)) RETURNS tinyint(1)
BEGIN
	if exists (select 1 from tbl_usr_userTokens where userID = _userID and userToken = _token) then
                                                        
		update tbl_usr_userTokens
        set lastupdate = current_timestamp
        where userID = _userID and userToken = _token;
        
		return true; #token is valid
    else
		return false; #token is not valid
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_copyParamValuesFromAppID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_copyParamValuesFromAppID`(in _userID bigint, in _userToken nvarchar(100), in _sourceAppID int, in _destinationAppID int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from talkabout_db.tbl_cnf_apps where appID = _sourceAppID) then
			set lErrorCode = -58; -- appID doesn't exist
		elseif not exists(select 1 from talkabout_db.tbl_cnf_apps where appID = _destinationAppID) then
			set lErrorCode = -58; -- appID doesn't exist
		end if;
        
        if(lErrorCode = 0) then
			-- first delete all existing params
			delete from talkabout_db.tbl_cnf_appDynamicConfigurations
            where appID = _destinationAppID;
        
			-- copy from the source app
			insert into talkabout_db.tbl_cnf_appDynamicConfigurations(paramID, appID, paramValue)
            select paramID, _destinationAppID, paramValue
            from talkabout_db.tbl_cnf_appDynamicConfigurations
            where appID = _sourceAppID;
            
            -- record the delete activity
			call p_usr_recordActivity(_userID, 12, "", 9, _destinationAppID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_createEditApp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_createEditApp`(in _userID bigint, in _userToken nvarchar(100), in _appID int, in _appName varchar(45), in _appDesc varchar(100))
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		/*
        if exists(select 1 from talkabout_db.tbl_cnf_apps where appID = _appID) then
			set lErrorCode = -59; -- appID already exists
		end if;
        */
        
        if(lErrorCode = 0) then
            if exists(select 1 from talkabout_db.tbl_cnf_apps where appID = _appID) then -- edit
				update talkabout_db.tbl_cnf_apps
                set appName = _appName,
					appDesc = _appDesc
                where appID = _appID;
            else -- insert a new app
				insert into talkabout_db.tbl_cnf_apps(appID, appName, appDesc)
                select _appID, _appName, _appDesc;
            end if;
            
            -- record the activity
			call p_usr_recordActivity(_userID, 13, "", 9, _appID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_createEditParamValue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_createEditParamValue`(in _userID bigint, in _userToken nvarchar(100), in _paramID int, in _appID int, in _paramValue nvarchar(1000))
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from talkabout_db.tbl_cnf_params where paramID = _paramID) then
			set lErrorCode = -57; -- paramID doesn't exist
		elseif not exists(select 1 from talkabout_db.tbl_cnf_apps where appID = _appID) then
			set lErrorCode = -58; -- appID doesn't exist
		end if;
        
        if(lErrorCode = 0) then
            if exists(select 1 from talkabout_db.tbl_cnf_appDynamicConfigurations where paramID = _paramID and appID = _appID) then -- edit
				update talkabout_db.tbl_cnf_appDynamicConfigurations
                set paramValue = _paramValue
                where paramID = _paramID and appID = _appID;
            else -- insert a new param value
				insert into talkabout_db.tbl_cnf_appDynamicConfigurations(paramID, appID, paramValue)
                select _paramID, _appID, _paramValue;
            end if;
            
            -- record the delete activity
			call p_usr_recordActivity(_userID, 9, _paramValue, 8, _paramID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_deleteAllParamValuesByAppID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_deleteAllParamValuesByAppID`(in _userID bigint, in _userToken nvarchar(100), in _appID int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from talkabout_db.tbl_cnf_apps where appID = _appID) then
			set lErrorCode = -58; -- appID doesn't exist
		end if;
        
        if(lErrorCode = 0) then
			delete from talkabout_db.tbl_cnf_appDynamicConfigurations
            where appID = _appID;
            
            -- record the delete activity
			call p_usr_recordActivity(_userID, 11, "", 9, _appID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_deleteApp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_deleteApp`(in _userID bigint, in _userToken nvarchar(100), in _appID int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from talkabout_db.tbl_cnf_apps where appID = _appID) then
			set lErrorCode = -58; -- appID doesn't exist
		end if;
        
        if(lErrorCode = 0) then
			-- delete the app configurations
			delete from talkabout_db.tbl_cnf_appDynamicConfigurations
            where appID = _appID;
            
            -- delete the app
            delete from talkabout_db.tbl_cnf_apps
            where appID = _appID;
            
            -- record the delete activity
			call p_usr_recordActivity(_userID, 14, "", 9, _appID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_deleteParamValue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_deleteParamValue`(in _userID bigint, in _userToken nvarchar(100), _paramID int, in _appID int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from talkabout_db.tbl_cnf_params where paramID = _paramID) then
			set lErrorCode = -57; -- paramID doesn't exist
		elseif not exists(select 1 from talkabout_db.tbl_cnf_apps where appID = _appID) then
			set lErrorCode = -58; -- appID doesn't exist
		end if;
        
        if(lErrorCode = 0) then
			delete from talkabout_db.tbl_cnf_appDynamicConfigurations
            where paramID = _paramID 
				and appID = _appID;
            
            -- record the delete activity
			call p_usr_recordActivity(_userID, 10, "", 8, _paramID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_getApps` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_getApps`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
		select appID,
               appName,
               appDesc
		from talkabout_db.tbl_cnf_apps
		order by appID;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_getConfigurations` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_getConfigurations`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
		select p.paramID,
			   p.paramName,
               p.paramDesc,
               pt.paramTypeID,
               pt.paramTypeName,
               pt.paramTypeDesc,
               pv.paramValue,
               a.appID,
               a.appName
		from talkabout_db.tbl_cnf_appDynamicConfigurations pv
			inner join talkabout_db.tbl_cnf_params p
				on pv.paramID = p.paramID
			inner join talkabout_db.tbl_cnf_paramTypes pt
				on p.paramTypeID = pt.paramTypeID
			inner join talkabout_db.tbl_cnf_apps a
				on pv.appID = a.appID
		order by a.appID, p.paramID asc;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_getParams` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_getParams`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
		select p.paramID,
			   p.paramName,
               p.paramDesc,
               pt.paramTypeID,
               pt.paramTypeName,
               pt.paramTypeDesc
		from talkabout_db.tbl_cnf_params p
			inner join talkabout_db.tbl_cnf_paramTypes pt
				on p.paramTypeID = pt.paramTypeID
		order by p.paramID asc;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_createContentFromCSV` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_createContentFromCSV`()
BEGIN
    declare done int;
    
    -- all parameters from the data table (fetched in the cursor)
    declare _quizCategoryName nvarchar(100);
    declare _quizSubjectName nvarchar(100);
    declare _quizName nvarchar(100);
    declare _quizFromPage int;
    declare _quizToPage int;
    declare _quizID bigint;
    declare _questionID bigint;
    declare _answerID int;
    declare _questionText nvarchar(100);
    declare _answerText nvarchar(100);
    declare _correctAnswer int;
    declare _wordHints nvarchar(100);
    
    -- 
    declare currentQuizID bigint;
    declare currentQuestionID bigint;
    
    -- 
    declare tempQuizCategoryID bigint;
    declare tempQuizSubjectID bigint;
    declare tempQuizID bigint;
    declare tempQuestionID bigint;
    
    declare cur1 cursor for select Category, `Subject`, `Name`, `From`, `To`, QuizID, QuestionID, AnswerID, QuestionText, AnswerText, CorrectAnswer, WordHints 
							from talkabout_db_admin.quizDataFormat
                            order by QuizID, QuestionID, AnswerID;
	declare continue handler for not found set done=1;

	set done = 0;
    
    set currentQuizID = 0;
    set currentQuestionID = 0;
    set tempQuizID = 0;
    set tempQuestionID = 0;
    
	open cur1;
		igmLoop: loop
			fetch cur1 into _quizCategoryName, _quizSubjectName, _quizName, _quizFromPage, _quizToPage, _quizID, _questionID, _answerID, _questionText, _answerText, _correctAnswer, _wordHints;
			if done = 1 then leave igmLoop; end if;

			if exists(select 1 from talkabout_db.tbl_quiz_categories where quizCategoryName = _quizCategoryName and teacherID = 0) then
				set tempQuizCategoryID = (select quizCategoryID from talkabout_db.tbl_quiz_categories where quizCategoryName = _quizCategoryName and teacherID = 0);
                
                if exists(select 1 from talkabout_db.tbl_quiz_subjects where quizCategoryID = tempQuizCategoryID and quizSubjectName = _quizSubjectName and teacherID = 0) then
					set tempQuizSubjectID = (select quizSubjectID from talkabout_db.tbl_quiz_subjects where quizCategoryID = tempQuizCategoryID and quizSubjectName = _quizSubjectName and teacherID = 0);
                    
                    if(currentQuizID != _quizID) then
						set currentQuizID = _quizID;
						
                        insert into talkabout_db.tbl_quiz_quizzes(quizSubjectID, teacherID, quizName, quizTypeID, quizIcon, fromPage, toPage)
						select tempQuizSubjectID, 0, _quizName, 1, "default", _quizFromPage, _quizToPage;
						
						set tempQuizID = LAST_INSERT_ID();
					end if;
                    
                    if(currentQuestionID != _questionID) then
						set currentQuestionID = _questionID;
						
                        insert into talkabout_db.tbl_quiz_quizQuestions(questionText, quizID, correctAnswerID, questionWordHints, correctAnswerAudioURL)
						select _questionText, tempQuizID, _correctAnswer, _wordHints, "";
						
						set tempQuestionID = LAST_INSERT_ID();
					end if;
                    
                    insert into talkabout_db.tbl_quiz_quizQuestionsAnswers(questionID, answerID, answerText)
					select tempQuestionID, _answerID, _answerText;
				else
					select -2;
				end if;
			else
				select -1;
            end if;
		end loop igmLoop;
	close cur1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_dash_getBasicValues` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_dash_getBasicValues`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
  declare lActiveStudentPeriod int;
  declare lActiveTeacherPeriod int;
  declare lNewQuizzesPeriod int;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;

    select count(1) as studentCount from talkabout_db.tbl_usr_users where userTypeID = 2; -- general student count
    select count(1) as teacherCount from talkabout_db.tbl_usr_users where userTypeID = 1; -- general teacher count
    
    set lActiveStudentPeriod = (select paramValue from talkabout_db.tbl_cnf_appDynamicConfigurations where paramID = 21 and appID = 0);
    set lActiveTeacherPeriod = (select paramValue from talkabout_db.tbl_cnf_appDynamicConfigurations where paramID = 22 and appID = 0);
    set lNewQuizzesPeriod = (select paramValue from talkabout_db.tbl_cnf_appDynamicConfigurations where paramID = 23 and appID = 0);

    select count(1) as activeStudentCount
    from talkabout_db.tbl_usr_users u
      inner join talkabout_db.tbl_usr_userTokens ut
        on u.userID = ut.userID
    where u.userTypeID = 2
      and ut.lastUpdate >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -lActiveStudentPeriod DAY); -- active student count

    select count(1) as activeTeacherCount
    from talkabout_db.tbl_usr_users u
      inner join talkabout_db.tbl_usr_userTokens ut
        on u.userID = ut.userID
    where u.userTypeID = 1
      and ut.lastUpdate >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -lActiveTeacherPeriod DAY); -- active teacher count

    select count(1) as newQuizCount
    from talkabout_db.tbl_quiz_quizzes
    where teacherID > 0
      and quizCreateDate >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -lNewQuizzesPeriod DAY); -- new private quizzes count

    -- most popular quizzes
    select q.quizID, 
           c.quizCategoryID,
           c.quizCategoryName,
           qs.quizSubjectID,
           qs.quizSubjectName,
           avg(uqs.finalScoreAvg) as accuracy,
           avg(uqs.correctAnswersCount) as correctness,
           count(1) as quizUsageCount
    from talkabout_db.view_rpt_userQuizSummary uqs
      inner join talkabout_db.tbl_quiz_quizzes q
        on uqs.quizID = q.quizID
      inner join talkabout_db.tbl_quiz_subjects qs
        on q.quizSubjectID = qs.quizSubjectID
      inner join talkabout_db.tbl_quiz_categories c
        on qs.quizCategoryID = c.quizCategoryID
    group by q.quizID
    order by quizUsageCount desc
    limit 5;

    -- easiest quizzes
    select q.quizID, 
           c.quizCategoryID,
           c.quizCategoryName,
           qs.quizSubjectID,
           qs.quizSubjectName,
           avg(uqs.finalScoreAvg) as accuracy,
           avg(uqs.correctAnswersCount) as correctness,
           count(1) as quizUsageCount
    from talkabout_db.view_rpt_userQuizSummary uqs
      inner join talkabout_db.tbl_quiz_quizzes q
        on uqs.quizID = q.quizID
      inner join talkabout_db.tbl_quiz_subjects qs
        on q.quizSubjectID = qs.quizSubjectID
      inner join talkabout_db.tbl_quiz_categories c
        on qs.quizCategoryID = c.quizCategoryID
    group by q.quizID
    order by accuracy desc
    limit 5;

    -- hardest quizzes
    select q.quizID, 
           c.quizCategoryID,
           c.quizCategoryName,
           qs.quizSubjectID,
           qs.quizSubjectName,
           avg(uqs.finalScoreAvg) as accuracy,
           avg(uqs.correctAnswersCount) as correctness,
           count(1) as quizUsageCount
    from talkabout_db.view_rpt_userQuizSummary uqs
      inner join talkabout_db.tbl_quiz_quizzes q
        on uqs.quizID = q.quizID
      inner join talkabout_db.tbl_quiz_subjects qs
        on q.quizSubjectID = qs.quizSubjectID
      inner join talkabout_db.tbl_quiz_categories c
        on qs.quizCategoryID = c.quizCategoryID
    group by q.quizID
    order by accuracy asc
    limit 5;
    
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_dash_getGraphValues` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_dash_getGraphValues`(in _userID bigint, in _userToken nvarchar(100), in _fromDate timestamp)
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;

    select qs.userID, sequenceNumber, finalScoreAvg
    from talkabout_db.view_rpt_userQuizSummary qs
      inner join talkabout_db.tbl_usr_users u
        on u.userID = qs.userID
    where (u.createDate >= _fromDate or _fromDate is null)
    order by qs.userID, sequenceNumber;
    
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_init_initializeData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_init_initializeData`()
BEGIN
	insert into talkabout_db.tbl_err_ErrorTypes(errorCodeID, errorCodeName, errorCodeDescription, errorText)
    select -1, "user not found", "user not found", "user not found";
    
    insert into tbl_quiz_categories(quizCategoryID, teacherID, quizCategoryName, quizCategoryIcon, quizCategoryColor)
    select 	0, 0, "Books", "books-stack-of-three", "3D325C";

	/*
	talkabout_db.tbl_cnf_appDynamicConfigurations
	'1', '1', '14c2be97'
	'2', '1', '16981a8afe4e47b9'
	'3', '1', 'TalkAbout'
	'4', '1', '1'
	'5', '1', '2'
	'6', '1', '0.7'
	'7', '1', '30'
	'8', '1', '30'
	'9', '1', '[{\"from\": 1, \"to\": 10, \"AllowedDeviationPercent\": 0.5}, {\"from\": 11, \"to\": 100, \"AllowedDeviationPercent\": 0.4}, {\"from\": 101, \"to\": 1000000, \"AllowedDeviationPercent\": 0.3}]'
	'10', '1', '3'
	'11', '1', '1000'
	'12', '1', '3'
	'13', '1', '1000'
	'14', '1', 'AIzaSyClQTf7-MZtIsQdzu6nxJ0kpUyUPZ0uGdY'
	'15', '1', 'talkabout-e2cab'
	'16', '1', 'facca393-a435-4cc7-a6ed-051e8346144e'
	'17', '1', 'ibKIGXjBFa3M'
	'18', '1', 'AKIAIFFA5U2CMSGBE3AQ'
	'19', '1', 'imREBpxCLeYQfuY96wmEt5bPpHWFWDcJU4jWcVES'
	'20', '1', '5000'
	*/














END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_addMemberToGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_addMemberToGroup`(in _userID bigint, in _userToken nvarchar(100), in _teacherUserID bigint, in _groupID bigint, in _phoneNumber varchar(45))
BEGIN
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call talkabout_db.p_nwk_addMemberToGroup_internal(_teacherUserID, _groupID, _phoneNumber);
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_createGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_createGroup`(in _userID bigint, in _userToken nvarchar(100), in _teacherUserID bigint, in _groupName nvarchar(100), in _groupTypeID int)
BEGIN
	declare lErrorCode int;
    declare lGroupID bigint;
    declare lGroupCode varchar(10);
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		
        call talkabout_db.p_nwk_createGroup_internal(_teacherUserID, _groupName, _groupTypeID, lErrorCode, lGroupID, lGroupCode);
        
        if(lErrorCode = 0) then
			call p_usr_recordActivity(_userID, 6, "", 6, lGroupID);
            			
			select 0 as returnValue, "ok" as errorText;
            select lGroupID as groupID, lGroupCode as groupCode;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_deleteGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_deleteGroup`(in _userID bigint, in _userToken nvarchar(100), in _teacherUserID bigint, in _groupID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call talkabout_db.p_nwk_deleteGroup_internal(_teacherUserID, _groupID, lErrorCode);
        
        if(lErrorCode = 0) then
			call p_usr_recordActivity(_userID, 7, "", 6, _groupID);
            			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_getGroupMembers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_getGroupMembers`(in _userID bigint, in _userToken nvarchar(100), in _teacherUserID bigint, in _groupID bigint)
BEGIN
	declare lErrorCode int;
  set lErrorCode = 0;
    
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call talkabout_db.p_nwk_getGroupMembers_internal(_teacherUserID, _groupID, lErrorCode);
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_getTeacherGroups` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_getTeacherGroups`(in _userID bigint, in _userToken nvarchar(100), in _teacherID bigint, in _groupTypeID int)
BEGIN
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
	  select 0 as returnValue, "ok" as errorText;
            			
	  select groupID, groupTypeID, groupName
    from talkabout_db.tbl_nwk_groups
    where adminTeacherID = _teacherID
		  and (groupTypeID = _groupTypeID or _groupTypeID = 0);		
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_removeMembersFromGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_removeMembersFromGroup`(in _userID bigint, in _userToken nvarchar(100), in _teacherUserID bigint, in _groupID bigint, in _phoneNumber varchar(45))
BEGIN
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call talkabout_db.p_nwk_removeMembersFromGroup_internal(_teacherUserID, _groupID, _phoneNumber);
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_shareQuizWithGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_shareQuizWithGroup`(in _userID bigint, in _userToken nvarchar(100), in _teacherUserID bigint, in _groupID bigint, in _quizID bigint)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
  set lErrorCode = 0;
  
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call talkabout_db.p_nwk_shareQuizWithGroup_internal(_teacherUserID, _groupID, _quizID, lErrorCode);
        
    if(lErrorCode = 0) then
	    call p_usr_recordActivity(_userID, 28, "", 6, _groupID);
            			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_createEditAnswer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_createEditAnswer`(in _userID bigint, in _userToken nvarchar(100), in _questionID bigint, in _answerID int, in _answerText nvarchar(200), in _isLastItem int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from talkabout_db.tbl_quiz_quizQuestions where questionID = _questionID) then
			set lErrorCode = -21; -- question doesn't exist
		elseif(CHAR_LENGTH(_answerText) <= 0) then
		 	set lErrorCode = -22; -- answer text cannot be empty
        end if;
        
        if(lErrorCode = 0) then
			if exists(select 1 from talkabout_db.tbl_quiz_quizQuestionsAnswers where questionID = _questionID and answerID = _answerID) then -- answer exists, edit it
				if exists(select 1 from talkabout_db.tbl_quiz_quizQuestionsAnswers where questionID = _questionID and answerID = _answerID and answerText != _answerText) then -- some data changed, update
					update talkabout_db.tbl_quiz_quizQuestionsAnswers
                    set answerText = _answerText
                    where questionID = _questionID and answerID = _answerID;
                    
                    -- record the activity
                    call p_usr_recordActivity(_userID, 23, _answerText, 4, _questionID);
                end if;
            else -- insert a new answer
				insert into talkabout_db.tbl_quiz_quizQuestionsAnswers(questionID, answerID, answerText)
				select _questionID, _answerID, _answerText;
                
                -- record the activity
                call p_usr_recordActivity(_userID, 22, _answerText, 4, _questionID);
			end if;
            
            if(_isLastItem > 0) then -- this is the last item, delete every answer remaining after this one
				delete from talkabout_db.tbl_quiz_quizQuestionsAnswers
                where questionID = _questionID and answerID > _isLastItem; -- _isLastItem is the biggest answerID for this question
            end if;

			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_createEditQuestion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_createEditQuestion`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint, in _questionID bigint, in _questionText nvarchar(200), in _correctAnswerID int, in _questionWordHints nvarchar(200))
BEGIN
	declare lErrorCode int;
    declare lNewQuestionID bigint;
    set lErrorCode = 0;
    set lNewQuestionID = 0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists(select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		elseif (not exists(select 1 from talkabout_db.tbl_quiz_quizQuestions where questionID = _questionID)) and (_questionID > 0) then
			set lErrorCode = -21; -- question doesn't exist
		elseif (not exists(select 1 from talkabout_db.tbl_quiz_quizQuestions where quizID = _quizID and questionID = _questionID)) and (_questionID > 0) then
			set lErrorCode = -63; -- question doesn't belong to quiz
		elseif(CHAR_LENGTH(_questionText) <= 0) then
			set lErrorCode = -19; -- question text cannot be empty
		elseif(_correctAnswerID <= 0) then
			set lErrorCode = -20; -- question must have a correct answer
        end if;
        
        if(lErrorCode = 0) then
			if(_questionID > 0) then -- edit existing question
				if exists(select 1 from talkabout_db.tbl_quiz_quizQuestions where questionID = _questionID
																and (questionText != _questionText or
																	 correctAnswerID != _correctAnswerID or
                                                                     questionWordHints != _questionWordHints)) then -- some data changed, update
					update talkabout_db.tbl_quiz_quizQuestions
					set questionText = _questionText,
						correctAnswerID = _correctAnswerID,
						questionWordHints = _questionWordHints
					where questionID = _questionID;
                    
                    call p_usr_recordActivity(_userID, 21, "", 4, _questionID);
				end if;
            
				set lNewQuestionID = _questionID;
            else -- insert a new question
				insert into talkabout_db.tbl_quiz_quizQuestions(questionText, quizID, correctAnswerID, questionWordHints)
				select _questionText, _quizID, _correctAnswerID, _questionWordHints;
				
				set lNewQuestionID = LAST_INSERT_ID();
                
                call p_usr_recordActivity(_userID, 20, "", 4, lNewQuestionID);
            end if;
            
			select 0 as returnValue, "ok" as errorText;
            select lNewQuestionID as questionID;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_createQuizHierarchy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_createQuizHierarchy`(IN _userID BIGINT, IN _userToken NVARCHAR(100),
											  IN _quizCategoryID BIGINT, IN _quizCategoryName NVARCHAR(100), IN _quizCategoryIcon VARCHAR(100), IN _alwaysShow INT,
											  IN _quizSubjectID BIGINT, IN _quizSubjectName NVARCHAR(100), IN _quizSubjectIcon VARCHAR(100), IN _quizSubjectSubHeader VARCHAR(100),
											  IN _quizName NVARCHAR(100), IN _quizIcon VARCHAR(100), IN _quizTypeID INT, IN _quizFromPage INT, IN _quizToPage INT)
BEGIN
	DECLARE lErrorCode INT;
    DECLARE lCategoryID BIGINT;
    DECLARE lSubjectID BIGINT;
    DECLARE lNewQuizID BIGINT;
    SET lErrorCode = 0;
    SET lNewQuizID = 0;
    
	IF(fn_usr_validateUserToken(_userID, _userToken) > 0) THEN
		CALL talkabout_db.p_quiz_createQuizHierarchy_internal(0, 0,
															  _quizCategoryID, _quizCategoryName, _quizCategoryIcon, _alwaysShow,
															  _quizSubjectID, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader,
															  _quizName, _quizIcon, _quizTypeID, _quizFromPage, _quizToPage,
															  lErrorCode, lCategoryID, lSubjectID, lNewQuizID);
		
		IF(lErrorCode = 0) THEN
			SELECT 0 AS returnValue, "ok" AS errorText;
			SELECT lCategoryID AS quizCategoryID, lSubjectID AS quizSubjectID, lNewQuizID AS quizID;
            
            CALL p_usr_recordActivity(_userID, 15, "", 3, lNewQuizID);
		ELSE
			SELECT lErrorCode AS returnValue, "error" AS errorText;
		END IF;
    ELSE
		SELECT -8 AS returnValue, "error" AS errorText; -- invalid token
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_deleteQuiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_deleteQuiz`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists (select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		end if;
		
		if(lErrorCode = 0) then
			
      delete qa.*
    	from talkabout_db.tbl_quiz_quizQuestionsAnswers qa
    		inner join talkabout_db.tbl_quiz_quizQuestions qq 
    			on qa.questionID = qq.questionID
    	where qq.quizID = _quizID;
	
	    delete from talkabout_db.tbl_quiz_quizQuestions where quizID = _quizID;
			
			delete from talkabout_db.tbl_nwk_groupsToQuizzes where quizID = _quizID;
            
      delete from talkabout_db.tbl_nwk_teacherShareExceptions where quizID = _quizID;
			
			delete from talkabout_db.tbl_usr_userAnswerSpeak where quizID = _quizID;
			
			delete from talkabout_db.tbl_arcv_usr_userAnswerSpeak where quizID = _quizID;
			
			delete from talkabout_db.tbl_rpt_userQuizSummary where quizID = _quizID;
			
			delete from talkabout_db.tbl_arcv_rpt_userQuizSummary where quizID = _quizID;

      delete from talkabout_db.tbl_quiz_quizzes where quizID = _quizID;
			
			call p_usr_recordActivity(_userID, 27, "", 3, _quizID);
			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_DeleteQuizQuestion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_DeleteQuizQuestion`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint, in _questionID bigint)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists (select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		elseif not exists (select 1 from talkabout_db.tbl_quiz_quizQuestions where quizID = _quizID and questionID = _questionID) then
			set lErrorCode = -21; -- question doesn't exist
		end if;
		
		if(lErrorCode = 0) then
            call talkabout_db.p_quiz_DeleteQuizQuestion_internal(_quizID, _questionID);
            
            call p_usr_recordActivity(_userID, 25, "", 4, _questionID);
            
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_editCategory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_editCategory`(IN _userID BIGINT, IN _userToken NVARCHAR(100), IN _quizCategoryID BIGINT, IN _quizCategoryName NVARCHAR(100), IN _quizCategoryIcon VARCHAR(100), IN _quizCategoryColor VARCHAR(10), IN _alwaysShow INT)
BEGIN
	DECLARE lErrorCode INT;
	DECLARE lTeacherID BIGINT;
    SET lErrorCode = 0;
    
    IF(fn_usr_validateUserToken(_userID, _userToken) > 0) THEN
        IF NOT EXISTS(SELECT 1 FROM talkabout_db.tbl_quiz_categories WHERE quizCategoryID = _quizCategoryID) THEN
			SET lErrorCode = -11; -- category doesn't exist
		ELSE
			SET lTeacherID = (SELECT teacherID FROM talkabout_db.tbl_quiz_categories WHERE quizCategoryID = _quizCategoryID);
			IF EXISTS(SELECT 1 FROM talkabout_db.tbl_quiz_categories WHERE quizCategoryName = _quizCategoryName AND quizCategoryID <> _quizCategoryID AND teacherID = lTeacherID) THEN
				SET lErrorCode = -10; -- category with same name already exist for this teacher
            END IF;
		END IF;
        
        IF(lErrorCode = 0) THEN
            UPDATE talkabout_db.tbl_quiz_categories
            SET quizCategoryName = _quizCategoryName,
				quizCategoryIcon = _quizCategoryIcon,
                quizCategoryColor = _quizCategoryColor,
                alwaysShow = _alwaysShow
			WHERE quizCategoryID = _quizCategoryID;
            
            -- record the activity
			CALL p_usr_recordActivity(_userID, 16, "", 1, _quizCategoryID);
			
            SELECT 0 AS returnValue, "ok" AS errorText;
		ELSE
			SELECT lErrorCode AS returnValue, "error" AS errorText;
		END IF;
    ELSE
		SELECT -8 AS returnValue, "error" AS errorText; -- invalid token
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_editQuizBasicData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_editQuizBasicData`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint, in _quizName nvarchar(100), in _quizIcon varchar(100),
											in _fromPage int, in _toPage int, in _quizStatusTypeID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    declare lQuizSubjectID bigint;
    declare lQuizTypeID int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists(select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		elseif not exists(select 1 from talkabout_db.tbl_quiz_quizStatusTypes where quizStatusTypeID = _quizStatusTypeID) then
			set lErrorCode = -61; -- quiz status typeID doesn't exist
		else
			set lTeacherID = (select teacherID from talkabout_db.tbl_quiz_quizzes where quizID = _quizID);
            set lQuizSubjectID = (select quizSubjectID from talkabout_db.tbl_quiz_quizzes where quizID = _quizID);
            set lQuizTypeID = (select quizTypeID from talkabout_db.tbl_quiz_quizzes where quizID = _quizID);
			if exists(select 1 from talkabout_db.tbl_quiz_quizzes where quizSubjectID = lQuizSubjectID and quizName = _quizName and quizTypeID = lQuizTypeID and teacherID = lTeacherID) then
				set lErrorCode = -14; -- quiz with same name and type already exists in the subject
            end if;
		end if;
        
        if(lErrorCode = 0) then
            update talkabout_db.tbl_quiz_quizzes
            set quizName = _quizName,
				quizIcon = _quizIcon,
                fromPage = _fromPage,
                toPage = _toPage,
                quizStatusTypeID = _quizStatusTypeID
			where quizID = _quizID;
            
            -- record the activity
			call p_usr_recordActivity(_userID, 18, "", 3, _quizID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_editSubject` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_editSubject`(IN _userID BIGINT, IN _userToken NVARCHAR(100), IN _quizSubjectID BIGINT, IN _quizSubjectName NVARCHAR(100), IN _quizSubjectIcon VARCHAR(100), IN _quizSubjectSubHeader VARCHAR(100))
BEGIN
	DECLARE lErrorCode INT;
	DECLARE lTeacherID BIGINT;
    DECLARE lQuizCategoryID BIGINT;
    SET lErrorCode = 0;
    
    IF(fn_usr_validateUserToken(_userID, _userToken) > 0) THEN
        IF NOT EXISTS(SELECT 1 FROM talkabout_db.tbl_quiz_subjects WHERE quizSubjectID = _quizSubjectID) THEN
			SET lErrorCode = -13; -- subject doesn't exist
		ELSE
			SET lTeacherID = (SELECT teacherID FROM talkabout_db.tbl_quiz_subjects WHERE quizSubjectID = _quizSubjectID);
            SET lQuizCategoryID = (SELECT quizCategoryID FROM talkabout_db.tbl_quiz_subjects WHERE quizSubjectID = _quizSubjectID);
			IF EXISTS(SELECT 1 FROM talkabout_db.tbl_quiz_subjects WHERE quizCategoryID = lQuizCategoryID AND quizSubjectName = _quizSubjectName AND teacherID = lTeacherID) THEN
				SET lErrorCode = -12; -- subject with same name already exists in the category
            END IF;
		END IF;
        
        IF(lErrorCode = 0) THEN
            UPDATE talkabout_db.tbl_quiz_subjects
            SET quizSubjectName = _quizSubjectName,
				        quizSubjectIcon = _quizSubjectIcon,
                quizSubjectSubHeader = _quizSubjectSubHeader
			WHERE quizSubjectID = _quizSubjectID;
            
            -- record the activity
			CALL p_usr_recordActivity(_userID, 17, "", 2, _quizSubjectID);
			
            SELECT 0 AS returnValue, "ok" AS errorText;
		ELSE
			SELECT lErrorCode AS returnValue, "error" AS errorText;
		END IF;
    ELSE
		SELECT -8 AS returnValue, "error" AS errorText; -- invalid token
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getAllCorrectAnswers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getAllCorrectAnswers`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken)) then
		select 0 as returnValue, "ok" as errorText;
		
		select qq.quizID,
			   qq.questionID,
			   qa.answerID,
			   qa.answerText
		from talkabout_db.tbl_quiz_quizQuestions qq
			inner join talkabout_db.tbl_quiz_quizQuestionsAnswers qa
				on qq.questionID = qa.questionID
					and qq.correctAnswerID = qa.answerID;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getAllPublicCategories` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getAllPublicCategories`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
			select 0 as returnValue, "ok" as errorText;
			
      select quizCategoryID,
  				   quizCategoryName
			from talkabout_db.tbl_quiz_categories
      where teacherID = 0
			order by quizCategoryID;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getAllPublicSubjectsByCategory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getAllPublicSubjectsByCategory`(IN _userID BIGINT, IN _userToken NVARCHAR(100), IN _quizCategoryID BIGINT)
BEGIN
	DECLARE lErrorCode INT;
    SET lErrorCode = 0;
    
    IF(fn_usr_validateUserToken(_userID, _userToken) > 0) THEN
			SELECT 0 AS returnValue, "ok" AS errorText;
			
      SELECT quizSubjectID,
  				   quizSubjectName,
             quizSubjectSubHeader
			FROM talkabout_db.tbl_quiz_subjects
      WHERE quizCategoryID = _quizCategoryID
        AND teacherID = 0
			ORDER BY quizSubjectID;
  ELSE
		SELECT -8 AS returnValue, "error" AS errorText; -- invalid token
  END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getAllQuizzes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getAllQuizzes`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
			select 0 as returnValue, "ok" as errorText;
			
			-- get general categories data
			select distinct
  				   qc.quizCategoryID,
  				   qc.quizCategoryName,
  				   qs.quizSubjectID,
  				   qs.quizSubjectName,
  				   q.quizID,
             q.quizName,
             q.fromPage,
             q.toPage,
  				   q.quizTypeID,
             q.quizCreateDate,
             q.quizStatusTypeID,
             qst.quizStatusTypeName,
             q.teacherID,
             case 
              when (q.teacherID = 0) then "Public"
              else "private"
             end as quizLevel,
             case 
              when (gq.quizID is null) then "Draft"
              else "Shared"
             end as quizShared,
             ifnull(avg(uqsv.finalScoreAvg), 0) as accuracy,
             ifnull(avg(uqsv.correctAnswersCount), 0) as correctness,
             count(uqsv.sequenceNumber) as usageCount,
             count(uqs.sequenceNumber) as studentsUsageCount,
             ifnull(qstat.shareCount, 0) as shareCount,
             ifnull(qstat.saveSourceCount, 0) as saveSourceCount,
             ifnull(qstat.cancelCount, 0) as cancelCount
			from talkabout_db.tbl_quiz_categories qc
				inner join talkabout_db.tbl_quiz_subjects qs
					on qc.quizCategoryID = qs.quizCategoryID
				inner join talkabout_db.tbl_quiz_quizzes q
					on qs.quizSubjectID = q.quizSubjectID
        inner join talkabout_db.tbl_quiz_quizStatusTypes qst
          on qst.quizStatusTypeID = q.quizStatusTypeID
        left join (select distinct quizID from talkabout_db.tbl_nwk_groupsToQuizzes) gq
          on gq.quizID = q.quizID
        left join talkabout_db.view_rpt_userQuizSummary uqsv
          on uqsv.quizID = q.quizID
        left join talkabout_db.tbl_rpt_userQuizSummary uqs
          on uqs.quizID = q.quizID
            and uqsv.quizID = uqs.quizID
            and uqsv.userID = uqs.userID
            and uqsv.sequenceNumber = uqs.sequenceNumber
        left join talkabout_db.tbl_quiz_quizStatistics qstat
          on q.quizID = qstat.quizID
      group by q.quizID
			order by q.quizID;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getAllQuizzes_simple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getAllQuizzes_simple`(in _userID bigint, in _userToken nvarchar(100), in _quizTypeID int, in _quizStatusTypeID int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists(select 1 from talkabout_db.tbl_quiz_quizTypes where quizTypeID = _quizTypeID) then -- check if quiz type exists
			set lErrorCode = -15; -- quiz typeID doesn't exist
		elseif not exists(select 1 from talkabout_db.tbl_quiz_quizStatusTypes where quizStatusTypeID = _quizStatusTypeID) then
			set lErrorCode = -61; -- quiz status typeID doesn't exist
        end if;
        
        if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
			
			-- get general categories data
			select distinct
				   qc.quizCategoryID,
				   qc.quizCategoryName,
				   qc.quizCategoryIcon,
				   IFNULL(qc.quizCategoryColor, "") as quizCategoryColor,
				   qs.quizSubjectID,
				   qs.quizSubjectName,
				   qs.quizSubjectIcon,
				   q.quizID,
           q.quizName,
           q.fromPage,
           q.toPage,
				   q.quizTypeID,
				   q.quizIcon,
                   q.teacherID
			from talkabout_db.tbl_quiz_categories qc
				inner join talkabout_db.tbl_quiz_subjects qs
					on qc.quizCategoryID = qs.quizCategoryID
				inner join talkabout_db.tbl_quiz_quizzes q
					on qs.quizSubjectID = q.quizSubjectID
			where q.quizTypeID = _quizTypeID
                and q.quizStatusTypeID = _quizStatusTypeID
			order by qc.quizCategoryID, qs.quizSubjectID, q.quizID;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getQuestionStatistics` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getQuestionStatistics`(in _userID bigint, in _userToken nvarchar(100), in _questionID bigint)
BEGIN
  declare lErrorCode int;
  set lErrorCode = 0;
    
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists (select 1 from talkabout_db.tbl_quiz_quizQuestions where questionID = _questionID) then
			set lErrorCode = -21; -- question doesn't exist
		end if;

    if(lErrorCode = 0) then
  	  select 0 as returnValue, "ok" as errorText;
  
      select ifnull(uas.questionID, _questionID) as questionID, ifnull(avg(finalScore), 0) as avgFinalScore, ifnull(count(uas.questionID), 0) as questionCount, ifnull(count(qq.questionID), 0) as correctAnswersCount
      from talkabout_db.view_usr_userAnswerSpeak uas
        left join talkabout_db.tbl_quiz_quizQuestions qq
          on qq.questionID = uas.questionID
            and qq.correctAnswerID = uas.recognizedAnswerID
      where uas.questionID = _questionID;
  
      select a.questionID, a.answerID, count(uas.questionID) as answerCount
      from talkabout_db.tbl_quiz_quizQuestionsAnswers a
        left join talkabout_db.view_usr_userAnswerSpeak uas
          on a.questionID = uas.questionID
            and a.answerID = uas.recognizedAnswerID
      where a.questionID = _questionID
      group by a.answerID;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getQuiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getQuiz`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
	
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
        end if;
        
        if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
			
			call talkabout_db.p_quiz_getQuiz_internal(_quizID);
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getUniqueCorrectAnswersByText` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getUniqueCorrectAnswersByText`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken)) then
		select 0 as returnValue, "ok" as errorText;
		
		select distinct qa.answerText
		from talkabout_db.tbl_quiz_quizQuestions qq
			inner join talkabout_db.tbl_quiz_quizQuestionsAnswers qa
				on qq.questionID = qa.questionID
					and qq.correctAnswerID = qa.answerID;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_inactivateQuiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_inactivateQuiz`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
       if not exists (select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		end if;
		
		if(lErrorCode = 0) then
			update talkabout_db.tbl_quiz_quizzes 
			set quizStatusTypeID = 0
			where quizID = _quizID;
			
			delete from talkabout_db.tbl_nwk_groupsToQuizzes where quizID = _quizID;
            
            delete from talkabout_db.tbl_nwk_teacherShareExceptions where quizID = _quizID;
			
			delete from talkabout_db.tbl_usr_userAnswerSpeak where quizID = _quizID;
			
			delete from talkabout_db.tbl_arcv_usr_userAnswerSpeak where quizID = _quizID;
			
			delete from talkabout_db.tbl_rpt_userQuizSummary where quizID = _quizID;
			
			delete from talkabout_db.tbl_arcv_rpt_userQuizSummary where quizID = _quizID;
			
			call p_usr_recordActivity(_userID, 24, "", 3, _quizID);
			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_MakeQuizPublic` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_MakeQuizPublic`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lErrorCode int;
    declare _quizCategoryID bigint;
    declare _quizCategoryName nvarchar(100);
    declare _quizCategoryIcon varchar(100);
    declare _quizSubjectID bigint;
    declare _quizSubjectName nvarchar(100);
    declare _quizSubjectIcon varchar(100);
    declare _quizName nvarchar(100);
    declare _quizIcon varchar(100);
    declare _quizTypeID int;
    declare _quizFromPage int;
    declare _quizToPage int;
    declare lCategoryID bigint;
    declare lSubjectID bigint;
    declare lNewQuizID bigint;
    
    set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists (select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		elseif not exists(select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID and teacherID > 0) then
			set lErrorCode = -62; -- quiz is already public
		end if;
		
		if(lErrorCode = 0) then
            select c.quizCategoryID, c.quizCategoryName, c.quizCategoryIcon,
				   s.quizSubjectID, s.quizSubjectName, s.quizSubjectIcon,
                   q.quizName, q.quizIcon, q.quizTypeID, q.fromPage, q.toPage
			into   _quizCategoryID, _quizCategoryName, _quizCategoryIcon,
				   _quizSubjectID, _quizSubjectName, _quizSubjectIcon,
				   _quizName, _quizIcon, _quizTypeID, _quizFromPage, _quizToPage
            from talkabout_db.tbl_quiz_categories c
				inner join talkabout_db.tbl_quiz_subjects s
					on c.quizCategoryID = s.quizCategoryID
				inner join talkabout_db.tbl_quiz_quizzes q
					on q.quizSubjectID = s.quizSubjectID
			where q.quizID = _quizID;
        
			-- create the new public quiz from the private one, if needed it will create also the category and the subject
			call talkabout_db.p_quiz_createQuizHierarchy_internal(0, 0,
													 -1, _quizCategoryName, _quizCategoryIcon,
                                                     0, _quizSubjectName, _quizSubjectIcon,
                                                     _quizName, _quizIcon, _quizTypeID, _quizFromPage, _quizToPage,
                                                     lErrorCode, lCategoryID, lSubjectID, lNewQuizID);
            
            if(lErrorCode = 0) then
				-- update new quiz with the original quizID
                update talkabout_db.tbl_quiz_quizzes
                set originalQuizID = _quizID
                where quizID = lNewQuizID;
                
                call talkabout_db.p_quiz_copyQuestionsAndAnswers_internal(_quizID, lNewQuizID);
                
                call p_usr_recordActivity(_userID, 19, "", 3, lNewQuizID);
            
				select 0 as returnValue, "ok" as errorText;
				select lCategoryID as quizCategoryID, lSubjectID as quizSubjectID, lNewQuizID as quizID;
			else
				select lErrorCode as returnValue, "error" as errorText;
            end if;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_reactivateQuiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_reactivateQuiz`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lErrorCode int;
  set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
       if not exists (select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		end if;
		
		if(lErrorCode = 0) then
			update talkabout_db.tbl_quiz_quizzes 
			set quizStatusTypeID = 1
			where quizID = _quizID;
			
			call p_usr_recordActivity(_userID, 30, "", 3, _quizID);
			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_saveQuizCopyAs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_saveQuizCopyAs`(IN _userID BIGINT, IN _userToken NVARCHAR(100), IN _quizID BIGINT, IN _newQuizName NVARCHAR(100))
BEGIN
	DECLARE lErrorCode INT;
  DECLARE _quizCategoryID BIGINT;
  DECLARE _quizCategoryName NVARCHAR(100);
  DECLARE _quizCategoryIcon VARCHAR(100);
  DECLARE _alwaysShow INT;
  DECLARE _quizSubjectID BIGINT;
  DECLARE _quizSubjectName NVARCHAR(100);
  DECLARE _quizSubjectIcon VARCHAR(100);
  DECLARE _quizSubjectSubHeader VARCHAR(100);
  DECLARE _quizIcon VARCHAR(100);
  DECLARE _quizTypeID INT;
  DECLARE _quizFromPage INT;
  DECLARE _quizToPage INT;
  DECLARE lCategoryID BIGINT;
  DECLARE lSubjectID BIGINT;
  DECLARE lNewQuizID BIGINT;
  
  SET lErrorCode = 0;

	IF(fn_usr_validateUserToken(_userID, _userToken) > 0) THEN
        
    IF NOT EXISTS (SELECT 1 FROM talkabout_db.tbl_quiz_quizzes WHERE quizID = _quizID) THEN
			SET lErrorCode = -18; -- quiz doesn't exist
		END IF;
		
		IF(lErrorCode = 0) THEN
			
      SELECT c.quizCategoryID, c.quizCategoryName, c.quizCategoryIcon, c.alwaysShow,
		         s.quizSubjectID, s.quizSubjectName, s.quizSubjectIcon, s.quizSubjectSubHeader,
             q.quizIcon, q.quizTypeID, q.fromPage, q.toPage
			INTO _quizCategoryID, _quizCategoryName, _quizCategoryIcon, _alwaysShow,
				   _quizSubjectID, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader,
				   _quizIcon, _quizTypeID, _quizFromPage, _quizToPage
      FROM talkabout_db.tbl_quiz_categories c
				INNER JOIN talkabout_db.tbl_quiz_subjects s
					ON c.quizCategoryID = s.quizCategoryID
				INNER JOIN talkabout_db.tbl_quiz_quizzes q
					ON q.quizSubjectID = s.quizSubjectID
			WHERE q.quizID = _quizID;
        
			-- create the new quiz for the requesting teacher, if needed it will create also the category and the subject
			CALL talkabout_db.p_quiz_createQuizHierarchy_internal(0, 0,
              													                     -1, _quizCategoryName, _quizCategoryIcon, _alwaysShow,
                                                              0, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader,
                                                              _newQuizName, _quizIcon, _quizTypeID, _quizFromPage, _quizToPage,
                                                              lErrorCode, lCategoryID, lSubjectID, lNewQuizID);
            
      IF(lErrorCode = 0) THEN
				-- update original quizID
        UPDATE talkabout_db.tbl_quiz_quizzes
        SET originalQuizID = _quizID,
            quizStatusTypeID = 0
        WHERE quizID = lNewQuizID;
        
        CALL talkabout_db.p_quiz_copyQuestionsAndAnswers_internal(_quizID, lNewQuizID);
        
        CALL p_usr_recordActivity(_userID, 31, "", 3, _quizID);

        CALL talkabout_db.p_quiz_updateQuizStatistics(_quizID, 2);
            
				SELECT 0 AS returnValue, "ok" AS errorText;
				SELECT lCategoryID AS quizCategoryID, lSubjectID AS quizSubjectID, lNewQuizID AS quizID;
			ELSE
				SELECT lErrorCode AS returnValue, "error" AS errorText;
      END IF;
		ELSE
			SELECT lErrorCode AS returnValue, "error" AS errorText;
		END IF;
  ELSE
		SELECT -8 AS returnValue, "error" AS errorText; -- invalid token
  END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_rpt_getGroupGraphValues` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_rpt_getGroupGraphValues`(in _userID bigint, in _userToken nvarchar(100), in _groupID bigint)
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;

    select qs.userID, sequenceNumber, finalScoreAvg
    from talkabout_db.view_rpt_userQuizSummary qs
      inner join talkabout_db.tbl_usr_users u
        on u.userID = qs.userID
      inner join talkabout_db.tbl_usr_students s
        on u.userID = s.userID
      inner join talkabout_db.tbl_nwk_groupsToUsers gu
        on gu.objectID = s.studentID
      inner join talkabout_db.tbl_nwk_groups g
        on g.groupID = gu.groupID
          and g.groupTypeID = 2
    where g.groupID = _groupID
    order by qs.userID, sequenceNumber;
    
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_rpt_getGroupListBasicData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_rpt_getGroupListBasicData`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
	  select 0 as returnValue, "ok" as errorText;
			
	  select g.groupName,
           g.adminTeacherID,
           g.groupID,
           ifnull(avg(qs.finalScoreAvg), 0) as finalScoreAvg,
           count(qs.sequenceNumber) as usageCount,
           ifnull(sum(stat.cancelCount), 0) as stopCount,
           count(distinct s.userID) as studentCount,
           count(distinct case
                  when(s.gender = 1)
                  then s.userID
                  else null
                 end) as maleCount,
           count(distinct case
                  when(s.gender = 2)
                  then s.userID
                  else null
                 end) as femaleCount
    from talkabout_db.tbl_nwk_groups g
		left join talkabout_db.tbl_nwk_groupsToUsers gu
			on g.groupID = gu.groupID
      left join talkabout_db.tbl_nwk_groupsToQuizzes gq
        on g.groupID = gq.groupID
		left join talkabout_db.tbl_quiz_quizzes q
			on gq.quizID = q.quizID
      left join talkabout_db.tbl_quiz_quizStatistics stat
        on q.quizID = stat.quizID
		left join talkabout_db.tbl_usr_students s
			on s.studentID = gu.objectID
		left join talkabout_db.view_rpt_userQuizSummary qs
			on qs.userID = s.userID
				and qs.quizID = gq.quizID
    where g.groupTypeID = 2
    group by g.groupID;		
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_rpt_getGroupStudentsListBasicData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_rpt_getGroupStudentsListBasicData`(in _userID bigint, in _userToken nvarchar(100), in _groupID bigint)
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;

    select s.userID, 
           s.studentID, 
           s.firstName, 
           s.lastName,
           s.gender, 
           gt.genderTypeName, 
           ifnull(avg(qs.finalScoreAvg), 0) as finalScoreAvg,
           ifnull(count(qs.sequenceNumber), 0) as usageCount
		from talkabout_db.tbl_nwk_groupsToUsers gu
			inner join talkabout_db.tbl_usr_students s
				on s.studentID = gu.objectID
      inner join talkabout_db.tbl_usr_genderTypes gt
        on s.gender = gt.genderTypeID
			left join talkabout_db.view_rpt_userQuizSummary qs
        on qs.userID = s.userID
		where gu.groupID = 116
    group by s.userID;


    select s.userID,
           ifnull(count(ua.id), 0) as stopsCount
		from talkabout_db.tbl_nwk_groupsToUsers gu
			inner join talkabout_db.tbl_usr_students s
				on s.studentID = gu.objectID
      left join talkabout_db.tbl_usr_usersActivity ua
        on ua.userID = s.userID
          and ua.activityTypeID = 8
		where gu.groupID = 116
    group by s.userID;
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_rpt_getStudentGraphValues` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_rpt_getStudentGraphValues`(in _userID bigint, in _userToken nvarchar(100), in _studentUserID bigint)
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;

    select userID,
           finalScoreAvg
		from talkabout_db.view_rpt_userQuizSummary
		where userID = _studentUserID;
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_convertStudentToTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_convertStudentToTeacher`(in _userID bigint, in _userToken nvarchar(100), in _userIDToConvert bigint)
BEGIN
	declare lErrorCode int;
  declare lStudentID bigint;
  set lErrorCode = 0;
  
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists(select 1 from talkabout_db.tbl_usr_users where userID = _userIDToConvert) then
    	set lErrorCode = -1; -- user not found
    elseif not exists(select 1 from talkabout_db.tbl_usr_users where userID = _userIDToConvert and userTypeID = 2) then
    	set lErrorCode = -53; -- user is not a student
    end if;
        
    if(lErrorCode = 0) then
  	  set lStudentID = talkabout_db.fn_usr_getStudentIDByUserID(_userIDToConvert);
            
    	-- copy the data from the student to the teacher
    	insert into talkabout_db.tbl_usr_teachers(userID, firstName, lastName, birthYear, gender, phoneNumber)
      select userID, firstName, lastName, birthYear, gender, phoneNumber
      from talkabout_db.tbl_usr_students 
      where userID = _userIDToConvert;
      
      delete from talkabout_db.tbl_usr_students where userID = _userIDToConvert;
    	
    	delete gu.*
    	from talkabout_db.tbl_nwk_groupsToUsers gu
    		inner join talkabout_db.tbl_nwk_groups g
    			on gu.groupID = g.groupID
    				and g.groupTypeID = 2
    	where objectID = lStudentID;
              
      delete from talkabout_db.tbl_usr_userTokens where userID = _userIDToConvert;
    	delete from talkabout_db.tbl_usr_usersActivity where userID = _userIDToConvert;
    	delete from talkabout_db.tbl_usr_userAnswerSpeak where userID = _userIDToConvert;
    	delete from talkabout_db.tbl_rpt_userQuizSummary where userID = _userIDToConvert;
    	delete from talkabout_db.tbl_arcv_rpt_userQuizSummary where userID = _userIDToConvert;
      delete from talkabout_db.tbl_arcv_usr_userAnswerSpeak where userID = _userIDToConvert;
              
      -- update user type to teacher
      update talkabout_db.tbl_usr_users
      set userTypeID = 1
      where userID = _userIDToConvert;
    			
    	call p_usr_recordActivity(_userID, 5, "", 5, _userIDToConvert);
              			
    	select 0 as returnValue, "ok" as errorText;
    else
    	select lErrorCode as returnValue, "error" as errorText;
    end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_convertTeacherToStudent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_convertTeacherToStudent`(in _userID bigint, in _userToken nvarchar(100), in _userIDToConvert bigint)
BEGIN
	declare lErrorCode int;
  declare lTeacherID bigint;
  declare lPhoneNumber varchar(45);
  set lErrorCode = 0;

  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists(select 1 from talkabout_db.tbl_usr_users where userID = _userIDToConvert) then
    	set lErrorCode = -1; -- user not found
    elseif not exists(select 1 from talkabout_db.tbl_usr_users where userID = _userIDToConvert and userTypeID = 1) then
    	set lErrorCode = -9; -- teacher not found
    elseif not exists(select 1 from talkabout_db.tbl_usr_teachers where userID = _userIDToConvert) then
    	set lErrorCode = -9; -- teacher not found
    end if;
        
    if(lErrorCode = 0) then
  	  set lTeacherID = talkabout_db.fn_usr_getTeacherIDByUserID(_userIDToConvert);
      set lPhoneNumber = (select phoneNumber from talkabout_db.tbl_usr_teachers where teacherID = lTeacherID);
            
    	-- copy the data from the student to the teacher
    	insert into talkabout_db.tbl_usr_students(userID, firstName, lastName, birthYear, gender, phoneNumber)
      select userID, firstName, lastName, birthYear, gender, phoneNumber
      from talkabout_db.tbl_usr_teachers
      where userID = _userIDToConvert;
      
      delete from talkabout_db.tbl_usr_teachers where userID = _userIDToConvert;
      delete from talkabout_db.tbl_nwk_teacherShareExceptions where teacherID = lTeacherID;
      delete from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers where phoneNumber = lPhoneNumber;
  		
      -- teacher groups data
  		delete gq.*
  		from talkabout_db.tbl_nwk_groupsToQuizzes gq
  			inner join talkabout_db.tbl_nwk_groups g
  				on gq.groupID = g.groupID
  		 where g.adminTeacherID = lTeacherID;
  		 
  		delete gu.*
  		from talkabout_db.tbl_nwk_groupsToUsers gu
  			inner join talkabout_db.tbl_nwk_groups g
  				on gu.groupID = g.groupID
  		 where g.adminTeacherID = lTeacherID;
  		
  		delete from talkabout_db.tbl_nwk_groups where adminTeacherID = lTeacherID;

      -- teacher content data
      delete qa.*
    	from talkabout_db.tbl_quiz_quizQuestionsAnswers qa
    		inner join talkabout_db.tbl_quiz_quizQuestions qq 
    			on qa.questionID = qq.questionID
        inner join talkabout_db.tbl_quiz_quizzes q
          on q.quizID = qq.quizID
    	where q.teacherID = lTeacherID;

      delete qq.*
    	from talkabout_db.tbl_quiz_quizQuestions qq
        inner join talkabout_db.tbl_quiz_quizzes q
          on q.quizID = qq.quizID
    	where q.teacherID = lTeacherID;
  		
  		delete from talkabout_db.tbl_quiz_categories where teacherID = lTeacherID;
  		delete from talkabout_db.tbl_quiz_subjects where teacherID = lTeacherID;
  		delete from talkabout_db.tbl_quiz_quizzes where teacherID = lTeacherID;
              
      delete from talkabout_db.tbl_usr_userTokens where userID = _userIDToConvert;
    	delete from talkabout_db.tbl_usr_usersActivity where userID = _userIDToConvert;
    	delete from talkabout_db.tbl_usr_userAnswerSpeak where userID = _userIDToConvert;
      delete from talkabout_db.tbl_arcv_usr_userAnswerSpeak where userID = _userIDToConvert;
              
      -- update user type to teacher
      update talkabout_db.tbl_usr_users
      set userTypeID = 2
      where userID = _userIDToConvert;
    			
    	call p_usr_recordActivity(_userID, 29, "", 5, _userIDToConvert);
              			
    	select 0 as returnValue, "ok" as errorText;
    else
    	select lErrorCode as returnValue, "error" as errorText;
    end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_deletePreRegisteredTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_deletePreRegisteredTeacher`(in _userID bigint, in _userToken nvarchar(100), in _teacherPhoneNumber varchar(45))
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists(select 1 from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers where phoneNumber = _teacherPhoneNumber) then
			set lErrorCode = -65; -- phone number doesn't exist in pre register teacher list
		end if;
        
        if(lErrorCode = 0) then
			delete from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers
            where phoneNumber = _teacherPhoneNumber;
					
			call p_usr_recordActivity(_userID, 26, "", 7, _teacherPhoneNumber);
            			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_deleteUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_deleteUser`(in _userID bigint, in _userToken nvarchar(100), in _userIDToDetete bigint)
BEGIN
	declare lErrorCode int;
  declare lTeacherOrStudent int;
  declare lTeacherID bigint;
  declare lStudentID bigint;
  declare lPhoneNumber varchar(45);
  set lErrorCode = 0;
  
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists(select 1 from talkabout_db.tbl_usr_users where userID = _userIDToDetete) then
    	set lErrorCode = -1; -- user not found
    end if;

    if(lErrorCode = 0) then
    	set lTeacherOrStudent = talkabout_db.fn_usr_checkIfTeacherOrStudentByUserID(_userIDToDetete);
    	
    	if(lTeacherOrStudent = 1) then -- user is a teacher
    		set lTeacherID = talkabout_db.fn_usr_getTeacherIDByUserID(_userIDToDetete);

        if(lTeacherID > 0) then
          set lPhoneNumber = (select phoneNumber from talkabout_db.tbl_usr_teachers where teacherID = lTeacherID);
      		
          -- teacher basic data
      		delete from talkabout_db.tbl_usr_teachers where userID = _userIDToDetete;
          delete from talkabout_db.tbl_nwk_teacherShareExceptions where teacherID = lTeacherID;
          delete from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers where phoneNumber = lPhoneNumber;
      		
          -- teacher groups data
      		delete gq.*
      		from talkabout_db.tbl_nwk_groupsToQuizzes gq
      			inner join talkabout_db.tbl_nwk_groups g
      				on gq.groupID = g.groupID
      		 where g.adminTeacherID = lTeacherID;
      		 
      		delete gu.*
      		from talkabout_db.tbl_nwk_groupsToUsers gu
      			inner join talkabout_db.tbl_nwk_groups g
      				on gu.groupID = g.groupID
    		  where g.adminTeacherID = lTeacherID;
      		
      		delete gu.*
      		from talkabout_db.tbl_nwk_groupsToUsers gu
      			inner join talkabout_db.tbl_nwk_groups g
      				on gu.groupID = g.groupID
      					and g.groupTypeID = 1
      		where objectID = lTeacherID;
      		
      		delete from talkabout_db.tbl_nwk_groups where adminTeacherID = lTeacherID;
  
          -- teacher content data
          delete qa.*
        	from talkabout_db.tbl_quiz_quizQuestionsAnswers qa
        		inner join talkabout_db.tbl_quiz_quizQuestions qq 
        			on qa.questionID = qq.questionID
            inner join talkabout_db.tbl_quiz_quizzes q
              on q.quizID = qq.quizID
        	where q.teacherID = lTeacherID;
  
          delete qq.*
        	from talkabout_db.tbl_quiz_quizQuestions qq
            inner join talkabout_db.tbl_quiz_quizzes q
              on q.quizID = qq.quizID
        	where q.teacherID = lTeacherID;
      		
      		delete from talkabout_db.tbl_quiz_categories where teacherID = lTeacherID;
      		delete from talkabout_db.tbl_quiz_subjects where teacherID = lTeacherID;
      		delete from talkabout_db.tbl_quiz_quizzes where teacherID = lTeacherID;
        else
          set lErrorCode = -9; -- teacher not found
        end if;
    	else -- user is a student
    		set lStudentID = talkabout_db.fn_usr_getStudentIDByUserID(_userIDToDetete);

        if(lStudentID > 0) then
      		delete from talkabout_db.tbl_usr_students where userID = _userIDToDetete;
      		
      		delete gu.*
      		from talkabout_db.tbl_nwk_groupsToUsers gu
      			inner join talkabout_db.tbl_nwk_groups g
      				on gu.groupID = g.groupID
      					and g.groupTypeID = 2
      		where objectID = lStudentID;
        else
          set lErrorCode = -36; -- student not found
        end if;
    	end if;

      if(lErrorCode = 0) then
      	delete from talkabout_db.tbl_usr_users where userID = _userIDToDetete;
      	delete from talkabout_db.tbl_usr_userTokens where userID = _userIDToDetete;
      	delete from talkabout_db.tbl_usr_usersActivity where userID = _userIDToDetete;
  
      	delete from talkabout_db.tbl_usr_userAnswerSpeak where userID = _userIDToDetete;
      	delete from talkabout_db.tbl_arcv_usr_userAnswerSpeak where userID = _userIDToDetete;
      	delete from talkabout_db.tbl_rpt_userQuizSummary where userID = _userIDToDetete;
      	delete from talkabout_db.tbl_arcv_rpt_userQuizSummary where userID = _userIDToDetete;
      	
        -- record the login activity
      	call p_usr_recordActivity(_userID, 3, "", 5, _userIDToDetete);
      	
        select 0 as returnValue, "ok" as errorText;
      else
      	select lErrorCode as returnValue, "error" as errorText;
      end if;
    else
    	select lErrorCode as returnValue, "error" as errorText;
    end if;
  else
    select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_editUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_editUser`(in _userID bigint, in _userToken nvarchar(100), _userIDToEdit bigint, in _firstName nvarchar(50),
 in _lastName nvarchar(50), in _password varchar(100), in _gender int, in _birthYear int)
BEGIN
	declare lErrorCode int;
  declare lTeacherOrStudent int;
	declare lTeacherID bigint;
  declare lStudentID bigint;
  set lErrorCode = 0;
    
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists(select 1 from talkabout_db.tbl_usr_users where userID = _userIDToEdit) then
    	set lErrorCode = -1; -- user not found
    end if;
          
    if(lErrorCode = 0) then
    	set lTeacherOrStudent = talkabout_db.fn_usr_checkIfTeacherOrStudentByUserID(_userIDToEdit);
             
      -- update the password only if a value was given 
      if(CHAR_LENGTH(_password) > 0) then
        update talkabout_db.tbl_usr_users
        set userPassword = _password
        where userID = _userIDToEdit;
      end if;
              
      if(lTeacherOrStudent = 1) then -- user is a teacher
    		set lTeacherID = talkabout_db.fn_usr_getTeacherIDByUserID(_userIDToEdit);
                  
    		update talkabout_db.tbl_usr_teachers
        set firstName = _firstName,
            lastName = _lastName,
            birthYear = _birthYear,
            gender = _gender
        where teacherID = lTeacherID;
      else -- user is a student
    		set lStudentID = talkabout_db.fn_usr_getStudentIDByUserID(_userIDToEdit);
                  
    		update talkabout_db.tbl_usr_students
        set firstName = _firstName,
            lastName = _lastName,
            birthYear = _birthYear,
            gender = _gender
        where studentID = lStudentID;
      end if;
              
      -- record the delete activity
    	call p_usr_recordActivity(_userID, 8, "", 5, _userIDToEdit);
    	
      select 0 as returnValue, "ok" as errorText;
    else
    	select lErrorCode as returnValue, "error" as errorText;
    end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getAdminUsersActivities` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getAdminUsersActivities`(in _userID bigint, in _userToken nvarchar(100), in _specificUserID bigint, in _activityTypeID int, in _activityFromTime timestamp)
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
		select *
        from tbl_usr_usersActivity ua
			inner join tbl_usr_activityTypes atp
				on ua.activityTypeID = atp.activityTypeID
			inner join tbl_usr_activityObjects ao
				on ua.objectTypeID = ao.objectTypeID
		where (ua.userID = _specificUserID or _specificUserID = 0)
			and (atp.activityTypeID = _activityTypeID or _activityTypeID = 0)
            and (ua.activityTime >= _activityFromTime or _activityFromTime is null);
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getAllUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getAllUsers`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
    select *
    from (
			select ut.userTypeID,
             ut.userTypeName,
             t.firstName,
             t.lastName,
             year(CURRENT_TIMESTAMP) - t.birthYear as age,
             t.gender,
             gt.genderTypeName,
             0 as accuracy,
             0 as 'usage',
             0 as stops,
             ifnull(group_concat(g.groupName), "") as networks,
             u.createDate,
             u.userID,
             t.teacherID as teacherID,
             t.phoneNumber,
             g.groupCode
			from talkabout_db.tbl_usr_users u
				inner join talkabout_db.tbl_usr_teachers t
					on u.userID = t.userID
        inner join talkabout_db.tbl_usr_userTypes ut
          on ut.userTypeID = u.userTypeID
        inner join talkabout_db.tbl_usr_genderTypes gt
          on gt.genderTypeID = t.gender
        left join talkabout_db.tbl_nwk_groups g
          on g.adminTeacherID = t.teacherID
      group by u.userID

      union all

      select tmp.userTypeID, tmp.userTypeName, tmp.firstName, tmp.lastName, tmp.age, tmp.gender, tmp.genderTypeName, tmp.accuracy, tmp.`usage`, count(tmp.stops) as stops, tmp.networks, tmp.createDate, tmp.userID, tmp.teacherID, tmp.phoneNumber, tmp.groupCode 
      from (
        select ut.userTypeID,
               ut.userTypeName,
               s.firstName,
               s.lastName,
               year(CURRENT_TIMESTAMP) - s.birthYear as age,
               s.gender,
               gt.genderTypeName,
               ifnull(avg(qs.finalScoreAvg), 0) as accuracy,
               ifnull(count(qs.quizID) ,0) as 'usage',
               ua.id as stops,
               "" as networks,
               u.createDate,
               u.userID,
               ifnull(g.adminTeacherID, 0) as teacherID,
               s.phoneNumber,
               "" as groupCode
        from talkabout_db.tbl_usr_users u
        inner join talkabout_db.tbl_usr_students s
        on u.userID = s.userID
          inner join talkabout_db.tbl_usr_userTypes ut
            on ut.userTypeID = u.userTypeID
          inner join talkabout_db.tbl_usr_genderTypes gt
            on gt.genderTypeID = s.gender
          left join talkabout_db.view_rpt_userQuizSummary qs
            on qs.userID = s.userID
          left join talkabout_db.tbl_usr_usersActivity ua
            on ua.userID = u.userID
              and ua.activityTypeID = 8
          left join talkabout_db.tbl_nwk_groupsToUsers gu
            on gu.objectID = s.studentID
          left join talkabout_db.tbl_nwk_groups g
            on g.groupID = gu.groupID
        group by u.userID, ua.id) tmp
      group by tmp.userID
      ) allUsers
    order by allUsers.userID;
  else
    select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getAllUsers_simple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getAllUsers_simple`(in _userID bigint, in _userToken nvarchar(100), in _firstName nvarchar(50), in _phoneNumber varchar(45))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
		select *
		from
		(
			select u.userID, userTypeID, statusTypeID, createDate, teacherID as objectID, firstName, lastName, birthYear, gender, phoneNumber
			from talkabout_db.tbl_usr_users u
				inner join talkabout_db.tbl_usr_teachers t
					on u.userID = t.userID
			where (t.firstName = _firstName or _firstName = "")
				and (t.phoneNumber = _phoneNumber or _phoneNumber = "")
					
			union all
			
			select u.userID, userTypeID, statusTypeID, createDate, studentID as objectID, firstName, lastName, birthYear, gender, phoneNumber
			from talkabout_db.tbl_usr_users u
				inner join talkabout_db.tbl_usr_students s
					on u.userID = s.userID
			where (s.firstName = _firstName or _firstName = "")
				and (s.phoneNumber = _phoneNumber or _phoneNumber = "")
		) a
		order by userID desc;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getPreRegisterTeachers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getPreRegisterTeachers`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
		select phoneNumber
		from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers
        order by phoneNumber;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getUserCardBasicValuesStudent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getUserCardBasicValuesStudent`(in _userID bigint, in _userToken nvarchar(100), in _userCardUserID bigint)
BEGIN
	declare lErrorCode int;
  declare lObjectID bigint;
  set lErrorCode = 0;

  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists (select 1 from talkabout_db.tbl_usr_users where userID = _userCardUserID) then
			set lErrorCode = -1; -- user not found
		end if;
		
		if(lErrorCode = 0) then
  		set lObjectID = talkabout_db.fn_usr_getStudentIDByUserID(_userCardUserID);
  		
      select 0 as returnValue, "ok" as errorText;
  
      -- quizAssignmentCompletions
      select count(1) as quizAssignmentCompletions
      from (
        select distinct userID, qs.quizID
        from talkabout_db.view_rpt_userQuizSummary qs
          inner join talkabout_db.tbl_nwk_groupsToQuizzes gq
            on gq.quizID = qs.quizID
          inner join talkabout_db.tbl_nwk_groupsToUsers gu
            on gu.groupID = gq.groupID
              and gu.objectID = lObjectID
        where qs.userID = _userCardUserID
      ) as tmp;
  
      -- quizContentCompletions
      select count(1) as quizContentCompletions
      from (
        select distinct userID, quizID
        from talkabout_db.view_rpt_userQuizSummary
        where userID = _userCardUserID
      ) as tmp;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getUserCardBasicValuesTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getUserCardBasicValuesTeacher`(in _userID bigint, in _userToken nvarchar(100), in _userCardUserID bigint)
BEGIN
	declare lErrorCode int;
  declare lTeacherID bigint;
  set lErrorCode = 0;

  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    set lTeacherID = talkabout_db.fn_usr_getTeacherIDByUserID(_userCardUserID);

    if not exists (select 1 from talkabout_db.tbl_usr_users where userID = _userCardUserID) then
			set lErrorCode = -1; -- user not found
  	elseif(lTeacherID = 0) then
  		set lErrorCode = -9; -- teacher not found
		end if;
		
		if(lErrorCode = 0) then
      select 0 as returnValue, "ok" as errorText;

      select count(1) as quizCreationCount from talkabout_db.tbl_quiz_quizzes where teacherID = lTeacherID;
      select count(1) as studentGroupsCount from talkabout_db.tbl_nwk_groups where adminTeacherID = lTeacherID and groupTypeID = 2;
      select count(1) as teacherGroupsCount from talkabout_db.tbl_nwk_groups where adminTeacherID = lTeacherID and groupTypeID = 1;

      select avg(qs.finalScoreAvg) as averageAccuracy
      from talkabout_db.view_rpt_userQuizSummary qs
        inner join talkabout_db.tbl_nwk_groupsToQuizzes gq
          on gq.quizID = qs.quizID
        inner join talkabout_db.tbl_nwk_groupsToUsers gu
          on gu.groupID = gq.groupID
        inner join talkabout_db.tbl_nwk_groups g
          on g.groupID = gu.groupID
            and g.groupTypeID = 2
        inner join talkabout_db.tbl_usr_students s
          on s.studentID = gu.objectID
            and s.userID = qs.userID
      where g.adminTeacherID = lTeacherID;

      select GROUP_CONCAT(tmp.birthYear) as ageGroups
      from (
        select distinct s.birthYear
        from talkabout_db.tbl_nwk_groupsToUsers gu
          inner join talkabout_db.tbl_nwk_groups g
            on gu.groupID = g.groupID
          inner join talkabout_db.tbl_usr_students s
            on gu.objectID = s.studentID
         where g.adminTeacherID = lTeacherID
      ) tmp;

      -- # of completed quizzes assigned to group
      select tmp.groupID, count(1) as completedQuizzesCount
      from
        (
          select distinct g.groupID, gu.objectID, qs.quizID
          from talkabout_db.view_rpt_userQuizSummary qs
            inner join talkabout_db.tbl_nwk_groupsToQuizzes gq
              on gq.quizID = qs.quizID
            inner join talkabout_db.tbl_nwk_groupsToUsers gu
              on gu.groupID = gq.groupID
            inner join talkabout_db.tbl_nwk_groups g
              on g.groupID = gu.groupID
                and g.groupTypeID = 2
            inner join talkabout_db.tbl_usr_students s
              on s.studentID = gu.objectID
                and s.userID = qs.userID
          where g.adminTeacherID = lTeacherID
          group by g.groupID, gu.objectID, qs.quizID
        ) tmp
       group by tmp.groupID;

      -- # of quizzes assigned to each group
      select distinct g.groupID, count(quizID) as assignedQuizzesCount
      from talkabout_db.tbl_nwk_groups g
        left join talkabout_db.tbl_nwk_groupsToQuizzes gq
          on g.groupID = gq.groupID
            and g.groupTypeID = 2
      where g.adminTeacherID = lTeacherID
      group by g.groupID
      order by g.groupID;

      -- # of students in each group
      select distinct g.groupID, count(gu.objectID) as studentCount
      from talkabout_db.tbl_nwk_groups g
        left join talkabout_db.tbl_nwk_groupsToUsers gu
          on g.groupID = gu.groupID
            and g.groupTypeID = 2
      where g.adminTeacherID = lTeacherID
      group by g.groupID
      order by g.groupID;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getUserCardGraphValuesStudent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getUserCardGraphValuesStudent`(in _userID bigint, in _userToken nvarchar(100), in _userCardUserID bigint, in _numOfDays int)
BEGIN
	declare lErrorCode int;
  set lErrorCode = 0;

  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists (select 1 from talkabout_db.tbl_usr_users where userID = _userCardUserID) then
			set lErrorCode = -1; -- user not found
		end if;
		
		if(lErrorCode = 0) then
      select 0 as returnValue, "ok" as errorText;

      -- number of quiz completions per day
      select DATE_FORMAT(reportTime, '%Y-%m-%d') as activityTime, count(1) as activityCount
      from talkabout_db.view_rpt_userQuizSummary
      where userID = _userCardUserID
        and reportTime >= DATE_ADD(CURRENT_TIMESTAMP, interval -_numOfDays day)
      group by year(reportTime), month(reportTime), day(reportTime);

      -- number of quiz cancels per day
      select DATE_FORMAT(activityTime, '%Y-%m-%d') as activityTime, count(1) as activityCount
      from talkabout_db.tbl_usr_usersActivity
      where activityTypeID = 8
        and userID = _userCardUserID
        and activityTime >= DATE_ADD(CURRENT_TIMESTAMP, interval -_numOfDays day)
      group by year(activityTime), month(activityTime), day(activityTime);
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getUserCardGraphValuesTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getUserCardGraphValuesTeacher`(in _userID bigint, in _userToken nvarchar(100), in _userCardUserID bigint, in _numOfDays int)
BEGIN
	declare lErrorCode int;
  set lErrorCode = 0;

  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    if not exists (select 1 from talkabout_db.tbl_usr_users where userID = _userCardUserID) then
			set lErrorCode = -1; -- user not found
		end if;
		
		if(lErrorCode = 0) then
      select 0 as returnValue, "ok" as errorText;

      -- number of quiz assignments per day
      select DATE_FORMAT(activityTime, '%Y-%m-%d') as activityTime, count(1) as activityCount
      from talkabout_db.tbl_usr_usersActivity
      where activityTypeID = 15
        and userID = _userCardUserID
        and activityTime >= DATE_ADD(CURRENT_TIMESTAMP, interval -_numOfDays day)
      group by year(activityTime), month(activityTime), day(activityTime);
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getUsersActivities` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getUsersActivities`(in _userID bigint, in _userToken nvarchar(100), in _specificUserID bigint, in _activityTypeID int, in _activityFromTime timestamp)
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
		select *
        from talkabout_db.tbl_usr_usersActivity ua
			inner join talkabout_db.tbl_usr_activityTypes atp
				on ua.activityTypeID = atp.activityTypeID
			inner join talkabout_db.tbl_usr_activityObjects ao
				on ua.objectTypeID = ao.objectTypeID
		where (ua.userID = _specificUserID or _specificUserID = 0)
			and (atp.activityTypeID = _activityTypeID or _activityTypeID = 0)
            and (ua.activityTime >= _activityFromTime or _activityFromTime is null);
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_login`(in _username nvarchar(45), in _userPassword varchar(100))
BEGIN
	declare currentUserID bigint;
	declare newToken varchar(100);
    set currentUserID = 0;
    
	if exists (select 1 from tbl_usr_users where username = _username and userPassword = _userPassword) then
		set currentUserID = (select userID from tbl_usr_users where username = _username and userPassword = _userPassword);
        
        set newToken = UUID();
        
        if exists (select 1 from tbl_usr_userTokens where userID = currentUserID) then
			update tbl_usr_userTokens
            set userToken = newToken
            where userID = currentUserID;
        else
			insert into tbl_usr_userTokens(userID, userToken)
            select currentUserID, newToken;
        end if;
        
        -- record the login activity
        call p_usr_recordActivity(currentUserID, 1, "", 0, 0);
        
        select 0 as returnValue, "ok" as errorText;
        select currentUserID as userID, newToken as userToken;
    else
		select -28 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_logout` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_logout`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		delete from tbl_usr_userTokens
        where userID = _userID;
            
		-- record the logout activity
        call p_usr_recordActivity(_userID, 2, "", 0, 0);
        
		select 0 as returnValue, "ok" as errorText;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_preRegisterTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_preRegisterTeacher`(in _userID bigint, in _userToken nvarchar(100), in _teacherPhoneNumber varchar(45))
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if exists(select 1 from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers where phoneNumber = _teacherPhoneNumber) then
			set lErrorCode = -52; -- phone number already in pre register teacher list
		end if;
        
        if(lErrorCode = 0) then
			insert into talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers(phoneNumber)
            select _teacherPhoneNumber;
					
			call p_usr_recordActivity(_userID, 4, "", 7, _teacherPhoneNumber);
            			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_recordActivity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_recordActivity`(in _userID bigint, in _activityTypeID int, in _freeText varchar(500), in _objectTypeID int, in _objectID bigint)
BEGIN
	-- record the user activity
	insert into tbl_usr_usersActivity(userID, activityTypeID, freeText, objectTypeID, objectID)
	select _userID, _activityTypeID, _freeText, _objectTypeID, _objectID;
    
    -- update the last update time of the main token
    update tbl_usr_userTokens
    set lastUpdate = now()
    where userID = _userID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_registerUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_registerUser`(in _firstName nvarchar(50), in _lastName nvarchar(50), in _password varchar(100),
                                                in _phoneNumber varchar(45), in _gender int, in _birthYear int, in _userTypeID int, in _groupCode varchar(10))
BEGIN
	declare lErrorCode int;
  declare lNewUserID bigint;
  declare lNewToken varchar(100);
  set lErrorCode = 0;
	set lNewUserID = 0;

  if exists (select 1 from talkabout_db.tbl_usr_students where phoneNumber = _phoneNumber) then
		set lErrorCode = -5; -- number already exists
	elseif exists (select 1 from talkabout_db.tbl_usr_teachers where phoneNumber = _phoneNumber) then
		set lErrorCode = -5; -- number already exists
  end if;

  if(lErrorCode = 0) then
    if(_userTypeID = 1) then -- request to register a teacher
      if not exists(select 1 from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers where phoneNumber = _phoneNumber) then -- pre register the number to ensure the user will be created as a teacher
        insert into talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers(phoneNumber)
        select _phoneNumber;
      end if;
    else -- request to register a student
      if exists(select 1 from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers where phoneNumber = _phoneNumber) then -- delete the pre registered number to ensure the user will be created as a student
        delete from talkabout_db.tbl_usr_preRegisterTeachersPhoneNumbers
        where phoneNumber = _phoneNumber;
      end if;
    end if;
  	
    call talkabout_db.p_usr_registerUser_internal(_firstName, _lastName, _password, _phoneNumber, _gender, _birthYear, _groupCode, lErrorCode, lNewUserID, lNewToken);
    if(lErrorCode = 0) then
      select lErrorCode as returnValue, "ok" as errorText;
      select lNewUserID as userID, lNewToken as userToken;
    else
      select lErrorCode as returnValue, "error" as errorText; 
    end if;
  else
    select lErrorCode as returnValue, "error" as errorText; 
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_validateUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_validateUser`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-23 21:50:31
