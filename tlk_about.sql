-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: dev-db.talkabout.site    Database: talkabout_db
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_arcv_rpt_userQuizSummary`
--

DROP TABLE IF EXISTS `tbl_arcv_rpt_userQuizSummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_arcv_rpt_userQuizSummary` (
  `userID` bigint(20) NOT NULL,
  `quizID` bigint(20) NOT NULL,
  `sequenceNumber` bigint(20) NOT NULL,
  `correctAnswersCount` int(11) NOT NULL DEFAULT '0',
  `audioConfidenceAvg` double(4,2) NOT NULL DEFAULT '0.00',
  `finalScoreAvg` double(4,2) NOT NULL DEFAULT '0.00',
  `reportTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`,`quizID`,`sequenceNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_arcv_usr_userAnswerSpeak`
--

DROP TABLE IF EXISTS `tbl_arcv_usr_userAnswerSpeak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_arcv_usr_userAnswerSpeak` (
  `sequenceNumber` bigint(20) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `questionID` bigint(20) NOT NULL,
  `quizID` bigint(20) NOT NULL,
  `recognizedAnswerID` int(11) NOT NULL,
  `audioTranscript` varchar(100) CHARACTER SET utf8 NOT NULL,
  `audioConfidence` double(4,2) NOT NULL,
  `finalScore` double(4,2) NOT NULL,
  `answerTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sequenceNumber`,`userID`,`questionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_cnf_appDynamicConfigurations`
--

DROP TABLE IF EXISTS `tbl_cnf_appDynamicConfigurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cnf_appDynamicConfigurations` (
  `paramID` int(11) NOT NULL,
  `appID` int(11) NOT NULL,
  `paramValue` varchar(1000) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`paramID`,`appID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_cnf_apps`
--

DROP TABLE IF EXISTS `tbl_cnf_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cnf_apps` (
  `appID` int(11) NOT NULL AUTO_INCREMENT,
  `appName` varchar(45) NOT NULL,
  `appDesc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`appID`)
) ENGINE=InnoDB AUTO_INCREMENT=3213124 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_cnf_paramTypes`
--

DROP TABLE IF EXISTS `tbl_cnf_paramTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cnf_paramTypes` (
  `paramTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `paramTypeName` varchar(45) NOT NULL,
  `paramTypeDesc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`paramTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_cnf_params`
--

DROP TABLE IF EXISTS `tbl_cnf_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cnf_params` (
  `paramID` int(11) NOT NULL AUTO_INCREMENT,
  `paramTypeID` int(11) NOT NULL,
  `paramName` varchar(45) NOT NULL,
  `paramDesc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`paramID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_err_ErrorTypes`
--

DROP TABLE IF EXISTS `tbl_err_ErrorTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_err_ErrorTypes` (
  `errorCodeID` int(11) NOT NULL,
  `errorCodeName` varchar(100) NOT NULL,
  `errorCodeDescription` varchar(500) NOT NULL,
  `errorText` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`errorCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_gen_verificationCodeTypes`
--

DROP TABLE IF EXISTS `tbl_gen_verificationCodeTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_gen_verificationCodeTypes` (
  `verificationTypeID` int(11) NOT NULL,
  `verificationTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`verificationTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nwk_groupTypes`
--

DROP TABLE IF EXISTS `tbl_nwk_groupTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nwk_groupTypes` (
  `groupTypeID` int(11) NOT NULL,
  `groupTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`groupTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nwk_groups`
--

DROP TABLE IF EXISTS `tbl_nwk_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nwk_groups` (
  `groupID` bigint(20) NOT NULL AUTO_INCREMENT,
  `adminTeacherID` bigint(20) NOT NULL,
  `groupTypeID` int(11) NOT NULL,
  `groupName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `groupCode` varchar(10) NOT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nwk_groupsToQuizzes`
--

DROP TABLE IF EXISTS `tbl_nwk_groupsToQuizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nwk_groupsToQuizzes` (
  `groupID` bigint(20) NOT NULL,
  `quizID` bigint(20) NOT NULL,
  PRIMARY KEY (`groupID`,`quizID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nwk_groupsToUsers`
--

DROP TABLE IF EXISTS `tbl_nwk_groupsToUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nwk_groupsToUsers` (
  `groupID` bigint(20) NOT NULL,
  `objectID` bigint(20) NOT NULL,
  PRIMARY KEY (`groupID`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nwk_teacherShareExceptions`
--

DROP TABLE IF EXISTS `tbl_nwk_teacherShareExceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nwk_teacherShareExceptions` (
  `teacherID` bigint(20) NOT NULL,
  `quizID` bigint(20) NOT NULL,
  PRIMARY KEY (`teacherID`,`quizID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_quiz_categories`
--

DROP TABLE IF EXISTS `tbl_quiz_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_quiz_categories` (
  `quizCategoryID` bigint(20) NOT NULL AUTO_INCREMENT,
  `teacherID` int(11) NOT NULL DEFAULT '0',
  `quizCategoryName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `quizCategoryIcon` varchar(100) NOT NULL,
  `quizCategoryCreateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quizCategoryColor` varchar(10) DEFAULT '6B588F',
  `quizCategoryModifyDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `alwaysShow` int(11) DEFAULT '1',
  PRIMARY KEY (`quizCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_quiz_quizQuestions`
--

DROP TABLE IF EXISTS `tbl_quiz_quizQuestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_quiz_quizQuestions` (
  `questionID` bigint(20) NOT NULL AUTO_INCREMENT,
  `questionText` varchar(200) CHARACTER SET utf8 NOT NULL,
  `quizID` bigint(20) NOT NULL,
  `correctAnswerID` int(11) NOT NULL,
  `questionWordHints` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `correctAnswerAudioURL` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`questionID`)
) ENGINE=InnoDB AUTO_INCREMENT=1847 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_quiz_quizQuestionsAnswers`
--

DROP TABLE IF EXISTS `tbl_quiz_quizQuestionsAnswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_quiz_quizQuestionsAnswers` (
  `questionID` bigint(20) NOT NULL,
  `answerID` int(11) NOT NULL,
  `answerText` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`questionID`,`answerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_quiz_quizStatistics`
--

DROP TABLE IF EXISTS `tbl_quiz_quizStatistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_quiz_quizStatistics` (
  `quizID` bigint(20) NOT NULL AUTO_INCREMENT,
  `shareCount` int(11) DEFAULT '0',
  `saveSourceCount` int(11) DEFAULT '0',
  `cancelCount` int(11) DEFAULT '0',
  PRIMARY KEY (`quizID`)
) ENGINE=InnoDB AUTO_INCREMENT=414 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_quiz_quizStatusTypes`
--

DROP TABLE IF EXISTS `tbl_quiz_quizStatusTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_quiz_quizStatusTypes` (
  `quizStatusTypeID` int(11) NOT NULL,
  `quizStatusTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`quizStatusTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_quiz_quizTypes`
--

DROP TABLE IF EXISTS `tbl_quiz_quizTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_quiz_quizTypes` (
  `quizTypeID` int(11) NOT NULL,
  `quizTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`quizTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_quiz_quizzes`
--

DROP TABLE IF EXISTS `tbl_quiz_quizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_quiz_quizzes` (
  `quizID` bigint(20) NOT NULL AUTO_INCREMENT,
  `quizSubjectID` bigint(20) NOT NULL,
  `teacherID` bigint(20) NOT NULL,
  `quizName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `quizTypeID` int(11) NOT NULL,
  `quizIcon` varchar(100) NOT NULL,
  `quizCreateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fromPage` int(11) NOT NULL DEFAULT '0',
  `toPage` int(11) NOT NULL DEFAULT '0',
  `originalQuizID` bigint(20) NOT NULL DEFAULT '0',
  `quizStatusTypeID` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`quizID`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_quiz_subjects`
--

DROP TABLE IF EXISTS `tbl_quiz_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_quiz_subjects` (
  `quizSubjectID` bigint(20) NOT NULL AUTO_INCREMENT,
  `quizCategoryID` bigint(20) NOT NULL,
  `teacherID` bigint(20) NOT NULL,
  `quizSubjectName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `quizSubjectIcon` varchar(100) NOT NULL,
  `quizSubjectCreateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quizSubjectModifyDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quizSubjectSubHeader` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`quizSubjectID`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_rpt_reportRanges`
--

DROP TABLE IF EXISTS `tbl_rpt_reportRanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rpt_reportRanges` (
  `id` int(11) NOT NULL,
  `from` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_rpt_userQuizSummary`
--

DROP TABLE IF EXISTS `tbl_rpt_userQuizSummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rpt_userQuizSummary` (
  `userID` bigint(20) NOT NULL,
  `quizID` bigint(20) NOT NULL,
  `sequenceNumber` bigint(20) NOT NULL,
  `correctAnswersCount` int(11) NOT NULL DEFAULT '0',
  `audioConfidenceAvg` double(4,2) NOT NULL DEFAULT '0.00',
  `finalScoreAvg` double(4,2) NOT NULL DEFAULT '0.00',
  `reportTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`,`quizID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_activityObjects`
--

DROP TABLE IF EXISTS `tbl_usr_activityObjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_activityObjects` (
  `objectTypeID` int(11) NOT NULL,
  `objectTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`objectTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_activityTypes`
--

DROP TABLE IF EXISTS `tbl_usr_activityTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_activityTypes` (
  `activityTypeID` int(11) NOT NULL,
  `activityTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`activityTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_genderTypes`
--

DROP TABLE IF EXISTS `tbl_usr_genderTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_genderTypes` (
  `genderTypeID` int(11) NOT NULL,
  `genderTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`genderTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_preRegisterTeachersPhoneNumbers`
--

DROP TABLE IF EXISTS `tbl_usr_preRegisterTeachersPhoneNumbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_preRegisterTeachersPhoneNumbers` (
  `phoneNumber` varchar(45) NOT NULL,
  PRIMARY KEY (`phoneNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_preRegisterUsersToGroup`
--

DROP TABLE IF EXISTS `tbl_usr_preRegisterUsersToGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_preRegisterUsersToGroup` (
  `phoneNumber` varchar(45) NOT NULL,
  `groupID` bigint(20) NOT NULL,
  PRIMARY KEY (`phoneNumber`,`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_statusTypes`
--

DROP TABLE IF EXISTS `tbl_usr_statusTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_statusTypes` (
  `statusTypeID` int(11) NOT NULL,
  `statusTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`statusTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_students`
--

DROP TABLE IF EXISTS `tbl_usr_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_students` (
  `studentID` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `firstName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lastName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `birthYear` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `phoneNumber` varchar(45) NOT NULL,
  PRIMARY KEY (`studentID`)
) ENGINE=InnoDB AUTO_INCREMENT=1256 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_teachers`
--

DROP TABLE IF EXISTS `tbl_usr_teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_teachers` (
  `teacherID` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `firstName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lastName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `birthYear` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `phoneNumber` varchar(45) NOT NULL,
  PRIMARY KEY (`teacherID`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_tempVerificationCodes`
--

DROP TABLE IF EXISTS `tbl_usr_tempVerificationCodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_tempVerificationCodes` (
  `phoneNumber` varchar(45) NOT NULL,
  `verificationTypeID` int(11) NOT NULL,
  `tempCode` varchar(45) NOT NULL,
  `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`phoneNumber`,`verificationTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_userAnswerSpeak`
--

DROP TABLE IF EXISTS `tbl_usr_userAnswerSpeak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_userAnswerSpeak` (
  `sequenceNumber` bigint(20) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `questionID` bigint(20) NOT NULL,
  `quizID` bigint(20) NOT NULL,
  `recognizedAnswerID` int(11) NOT NULL,
  `audioTranscript` varchar(100) CHARACTER SET utf8 NOT NULL,
  `audioConfidence` double(4,2) NOT NULL,
  `finalScore` double(4,2) NOT NULL,
  `answerTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sequenceNumber`,`userID`,`questionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_userTokens`
--

DROP TABLE IF EXISTS `tbl_usr_userTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_userTokens` (
  `userID` bigint(20) NOT NULL,
  `userToken` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_userTypes`
--

DROP TABLE IF EXISTS `tbl_usr_userTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_userTypes` (
  `userTypeID` int(11) NOT NULL,
  `userTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`userTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_users`
--

DROP TABLE IF EXISTS `tbl_usr_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_users` (
  `userID` bigint(20) NOT NULL AUTO_INCREMENT,
  `userPassword` varchar(50) CHARACTER SET utf8 NOT NULL,
  `userTypeID` int(11) NOT NULL,
  `statusTypeID` int(11) NOT NULL DEFAULT '1',
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=1233 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_usr_usersActivity`
--

DROP TABLE IF EXISTS `tbl_usr_usersActivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usr_usersActivity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `activityTypeID` int(11) NOT NULL,
  `freeText` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `objectTypeID` int(11) NOT NULL DEFAULT '0',
  `objectID` bigint(20) DEFAULT '0',
  `activityTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31503 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `view_rpt_userQuizSummary`
--

DROP TABLE IF EXISTS `view_rpt_userQuizSummary`;
/*!50001 DROP VIEW IF EXISTS `view_rpt_userQuizSummary`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_rpt_userQuizSummary` AS SELECT 
 1 AS `userID`,
 1 AS `quizID`,
 1 AS `sequenceNumber`,
 1 AS `correctAnswersCount`,
 1 AS `audioConfidenceAvg`,
 1 AS `finalScoreAvg`,
 1 AS `reportTime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_usr_userAnswerSpeak`
--

DROP TABLE IF EXISTS `view_usr_userAnswerSpeak`;
/*!50001 DROP VIEW IF EXISTS `view_usr_userAnswerSpeak`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_usr_userAnswerSpeak` AS SELECT 
 1 AS `sequenceNumber`,
 1 AS `userID`,
 1 AS `questionID`,
 1 AS `quizID`,
 1 AS `recognizedAnswerID`,
 1 AS `audioTranscript`,
 1 AS `audioConfidence`,
 1 AS `finalScore`,
 1 AS `answerTime`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'talkabout_db'
--

--
-- Dumping routines for database 'talkabout_db'
--
/*!50003 DROP FUNCTION IF EXISTS `fn_usr_checkIfTeacherOrStudentByUserID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` FUNCTION `fn_usr_checkIfTeacherOrStudentByUserID`(_userID bigint) RETURNS int(11)
BEGIN
	if exists(select 1 from tbl_usr_teachers where userID = _userID) then
		return 1;
    elseif exists(select 1 from tbl_usr_students where userID = _userID) then
		return 2;
    else
		return 0;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_usr_checkIfTeacherOrStudentGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` FUNCTION `fn_usr_checkIfTeacherOrStudentGroup`(_groupID bigint) RETURNS int(11)
BEGIN
	if exists(select 1 from tbl_nwk_groups where groupID = _groupID and groupTypeID = 1) then
    return 1;
  elseif exists(select 1 from tbl_nwk_groups where groupID = _groupID and groupTypeID = 2) then
    return 2;
  else
    return 0;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_usr_getStudentIDByUserID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` FUNCTION `fn_usr_getStudentIDByUserID`(_userID bigint) RETURNS bigint(20)
BEGIN
	declare lStudentID bigint;
    
	if exists(select 1 from tbl_usr_students where userID = _userID) then
		set lStudentID = (select studentID from tbl_usr_students where userID = _userID);
    else
		set lStudentID = 0;
    end if;
    
    return lStudentID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_usr_getTeacherIDByStudentID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` FUNCTION `fn_usr_getTeacherIDByStudentID`(_studentID bigint) RETURNS bigint(20)
BEGIN
	declare lTeacherID bigint;
    
    if exists(select 1 from tbl_nwk_groups g
				inner join tbl_nwk_groupsToUsers gu
					on g.groupID = gu.groupID
						and g.groupTypeID = 2
				where gu.objectID = _studentID) then
		set lTeacherID = (select adminTeacherID from tbl_nwk_groups g
							inner join tbl_nwk_groupsToUsers gu
								on g.groupID = gu.groupID
									and g.groupTypeID = 2
							where gu.objectID = _studentID);
	else
		set lTeacherID = 0;
    end if;
    
    return lTeacherID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_usr_getTeacherIDByUserID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` FUNCTION `fn_usr_getTeacherIDByUserID`(_userID bigint) RETURNS bigint(20)
BEGIN
	declare lTeacherID bigint;
    
	if exists(select 1 from tbl_usr_teachers where userID = _userID) then
		set lTeacherID = (select teacherID from tbl_usr_teachers where userID = _userID);
    else
		set lTeacherID = 0;
    end if;
    
    return lTeacherID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_usr_getUserIDByPhoneNumber` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` FUNCTION `fn_usr_getUserIDByPhoneNumber`(_userPhoneNumber varchar(45)) RETURNS bigint(20)
BEGIN
	declare lUserID bigint;
    set lUserID = 0;
    
    
	if exists(select 1 from tbl_usr_teachers where phoneNumber = _userPhoneNumber) then
		set lUserID = (select userID from tbl_usr_teachers where phoneNumber = _userPhoneNumber);
    elseif exists(select 1 from tbl_usr_students where phoneNumber = _userPhoneNumber) then
		set lUserID = (select userID from tbl_usr_students where phoneNumber = _userPhoneNumber);
    end if;
    
    return lUserID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_usr_validateUserToken` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` FUNCTION `fn_usr_validateUserToken`(_userID bigint, _token nvarchar(100)) RETURNS tinyint(1)
BEGIN
	if exists (select 1 from tbl_usr_userTokens where userID = _userID and userToken = _token) then
                                                        
		update tbl_usr_userTokens
        set lastupdate = current_timestamp
        where userID = _userID and userToken = _token;
        
		return true; #token is valid
    else
		return false; #token is not valid
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_admin_validateAdminUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_admin_validateAdminUser`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(talkabout_db_admin.fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_cnf_getConfigurationsByAppID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_cnf_getConfigurationsByAppID`(in _appID int)
BEGIN
	select 0 as returnValue, "ok" as errorText;
    
	select dc.paramID,
		   dc.paramValue,
           p.paramTypeID,
           p.paramName
    from tbl_cnf_appDynamicConfigurations dc
		inner join tbl_cnf_params p
			on dc.paramID = p.paramID
	where dc.appID = _appID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_err_getErrorList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_err_getErrorList`()
BEGIN
	select 0 as returnValue, "ok" as errorText;
	
	select errorCodeID,
		   ifnull(errorText, "general error") as errorText
	from tbl_err_ErrorTypes;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_addMemberToGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_addMemberToGroup`(in _userID bigint, in _userToken nvarchar(100), in _groupID bigint, in _phoneNumber varchar(45))
BEGIN
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call p_nwk_addMemberToGroup_internal(_userID, _groupID, _phoneNumber);
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_addMemberToGroup_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_addMemberToGroup_internal`(in _teacherUserID bigint, in _groupID bigint, in _phoneNumber varchar(45))
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
  declare lUserID bigint;
  declare lFoundTeacherID bigint;
  declare lFoundStudentID bigint;
  declare lIsRegistered int;
  set lErrorCode = 0;
  set lIsRegistered = 1;
    
  set lTeacherID = fn_usr_getTeacherIDByUserID(_teacherUserID);
        
  if(lTeacherID = 0) then
  	set lErrorCode = -9; -- teacher not found
  elseif not exists(select 1 from tbl_nwk_groups where groupID = _groupID) then
  	set lErrorCode = -45; -- group doesn't exist
  elseif not exists(select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID) then
  	set lErrorCode = -39; -- group doesn't belong to the teacher
  end if;
        
  if(lErrorCode = 0) then
	  set lUserID = fn_usr_getUserIDByPhoneNumber(_phoneNumber);
          
    if(lUserID > 0) then -- it is a registered user
  		if(fn_usr_checkIfTeacherOrStudentByUserID(lUserID) = 1) then -- the user is a teacher
  			if exists(select 1 from tbl_nwk_groups where groupID = _groupID and groupTypeID = 1) then -- adding a teacher to a teacher's group
  				set lFoundTeacherID = fn_usr_getTeacherIDByUserID(lUserID);
                    
  				if(lFoundTeacherID = lTeacherID) then 
  					set lErrorCode = -48; -- teacher tried to add himself to his group
  				elseif not exists(select 1 from tbl_nwk_groupsToUsers where objectID = lFoundTeacherID and groupID = _groupID) then
  					insert into tbl_nwk_groupsToUsers(groupID, objectID)
  					select _groupID, lFoundTeacherID;
  				else
  					set lErrorCode = -44; -- phone number already registered for the group
  				end if;
        else
  				set lErrorCode = -46; -- unable to add teacher into a student's group
        end if;
      else -- the user is a student
  			if exists(select 1 from tbl_nwk_groups where groupID = _groupID and groupTypeID = 2) then -- adding a student to a student's group
  				set lFoundStudentID = fn_usr_getStudentIDByUserID(lUserID);
                    
  				if exists(select 1 from tbl_nwk_groupsToUsers gu
      							inner join tbl_nwk_groups g on gu.groupID = g.groupID
      							where g.groupTypeID = 2 and gu.objectID = lFoundStudentID) then -- the student belongs to a group
  					if not exists(select 1 from tbl_nwk_groupsToUsers where objectID = lFoundStudentID and groupID = _groupID) then
  						set lErrorCode = -43; -- student can only belong to one group at a time
  					else
  						set lErrorCode = -44; -- phone number already registered for the group
  					end if;
  				else -- the student does not belong to any group 
  					insert into tbl_nwk_groupsToUsers(groupID, objectID)
  					select _groupID, lFoundStudentID;
          end if;
        else
  				set lErrorCode = -47; -- unable to add student into a teacher's group
        end if;
      end if;
    else -- no user is found
		  if exists(select 1 from tbl_usr_preRegisterUsersToGroup where phoneNumber = _phoneNumber) then
			  if not exists(select 1 from tbl_usr_preRegisterUsersToGroup where phoneNumber = _phoneNumber and groupID = _groupID) then
  			  if exists(select 1 from tbl_usr_preRegisterUsersToGroup pu
      						  inner join tbl_nwk_groups g on pu.groupID = g.groupID
    						    where phoneNumber = _phoneNumber and g.groupTypeID = 2) then -- the phone is already registered to a student's group
            set lErrorCode = -43; -- student can only belong to one group at a time
          else
            if(talkabout_db.fn_usr_checkIfTeacherOrStudentGroup(_groupID) = 1) then -- number already exists in a teacher group, can only add to another teacher group
              insert into tbl_usr_preRegisterUsersToGroup(phoneNumber, groupID)
    					select _phoneNumber, _groupID;
                      
    					set lIsRegistered = 0;
            else
              set lErrorCode = -47; -- unable to add student into a teacher's group
            end if;
          end if;
				else
					set lErrorCode = -44; -- phone number already registered for the group
		    end if;
      else -- user is not found and also not already registered
			  insert into tbl_usr_preRegisterUsersToGroup(phoneNumber, groupID)
        select _phoneNumber, _groupID;
                  
        set lIsRegistered = 0;
      end if;
    end if;
            
    if(lErrorCode = 0) then
  		call p_usr_recordActivity(_teacherUserID, 18, "", 6, _groupID);
                
  		select 0 as returnValue, "ok" as errorText;
      select lIsRegistered as isRegistered;
    else
  		select lErrorCode as returnValue, "error" as errorText;
  	end if;
  else
  	select lErrorCode as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_createGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_createGroup`(in _userID bigint, in _userToken nvarchar(100), in _groupName nvarchar(100), in _groupTypeID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    declare lGroupID bigint;
    declare lGroupCode varchar(10);
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call talkabout_db.p_nwk_createGroup_internal(_userID, _groupName, _groupTypeID, lErrorCode, lGroupID, lGroupCode);
        
        if(lErrorCode = 0) then
			call p_usr_recordActivity(_userID, 12, "", 6, lGroupID);
            			
			select 0 as returnValue, "ok" as errorText;
            select lGroupID as groupID, lGroupCode as groupCode;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_createGroup_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_createGroup_internal`(in _teacherUserID bigint, in _groupName nvarchar(100), in _groupTypeID int, out _errorCode int, out _newGroupID bigint, out _groupCode varchar(10))
BEGIN
	declare lTeacherID bigint;
  declare lExistingGroupCode varchar(10);
  declare lLastGroupNumber int;
  set _errorCode = 0;
  set _newGroupID = 0;
  set _groupCode = '';
    
	set lTeacherID = fn_usr_getTeacherIDByUserID(_teacherUserID);
	
	if(lTeacherID = 0) then
		set _errorCode = -9; -- teacher not found
	elseif (CHAR_LENGTH(_groupName) <= 0) then
		set _errorCode = -2; -- missing parameters
	elseif not exists(select 1 from tbl_nwk_groupTypes where groupTypeID = _groupTypeID) then
		set _errorCode = -41; -- group typeID doesn't exist
	elseif exists(select 1 from tbl_nwk_groups where groupName = _groupName and adminTeacherID = lTeacherID) then
		set _errorCode = -54; -- group with same name already exists for this teacher
  elseif (lTeacherID >= 10000) then
    set _errorCode = -69; -- cannot create group code, teacher count is over 10000
	end if;
	
	if(_errorCode = 0) then
    if(lTeacherID < 10) then
      set _groupCode = concat('000', lTeacherID);
    elseif (lTeacherID < 100) then
      set _groupCode = concat('00', lTeacherID);
    elseif (lTeacherID < 1000) then
      set _groupCode = concat('0', lTeacherID);
    elseif (lTeacherID < 10000) then
      set _groupCode = concat('', lTeacherID);
    end if;
    
    if exists(select 1 from tbl_nwk_groups where adminTeacherID = lTeacherID) THEN -- get the last group and increment by 1
      set lExistingGroupCode = (select groupCode from tbl_nwk_groups where adminTeacherID = lTeacherID order by groupID desc LIMIT 1);

      set lLastGroupNumber = cast(SUBSTRING(lExistingGroupCode, 5) as SIGNED) + 1;
      
      if(lLastGroupNumber < 10) then
        set _groupCode = concat(_groupCode, '0');
        set _groupCode = concat(_groupCode, lLastGroupNumber);
      else
        set _groupCode = concat(_groupCode, lLastGroupNumber);
      end if;
    else -- first group for the teacher
      set _groupCode = concat(_groupCode, '01');
    end if;

		insert into tbl_nwk_groups(adminTeacherID, groupTypeID, groupName, groupCode)
		select lTeacherID, _groupTypeID, _groupName, _groupCode;
		
		set _newGroupID = LAST_INSERT_ID();
				
		call p_usr_recordActivity(_teacherUserID, 12, "", 6, _newGroupID);
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_createShareQuizException` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_createShareQuizException`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
        if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists(select 1 from tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		end if;
        
        if(lErrorCode = 0) then
			if not exists(select 1 from tbl_nwk_teacherShareExceptions where teacherID = lTeacherID and quizID = _quizID) then
				insert into tbl_nwk_teacherShareExceptions(teacherID, quizID)
				select lTeacherID, _quizID;
			
				call p_usr_recordActivity(_userID, 16, "", 3, _quizID);
            end if;
            			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_deleteGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_deleteGroup`(in _userID bigint, in _userToken nvarchar(100), in _groupID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
  set lErrorCode = 0;
    
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call p_nwk_deleteGroup_internal(_userID, _groupID, lErrorCode);
        
    if(lErrorCode = 0) then
			call p_usr_recordActivity(_userID, 13, "", 6, _groupID);
            			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_deleteGroup_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_deleteGroup_internal`(in _teacherUserID bigint, in _groupID int, out _errorCode int)
BEGIN
	declare lTeacherID bigint;
    set _errorCode = 0;
    
	set lTeacherID = fn_usr_getTeacherIDByUserID(_teacherUserID);
	
	if(lTeacherID = 0) then
		set _errorCode = -9; -- teacher not found
	elseif not exists(select 1 from tbl_nwk_groups where groupID = _groupID) then
		set _errorCode = -45; -- group doesn't exist
	elseif not exists(select 1 from tbl_nwk_groups where adminTeacherID = lTeacherID and groupID = _groupID) then
		set _errorCode = -42; -- teacher can delete only his own groups
	end if;
	
	if(_errorCode = 0) then
		delete from tbl_nwk_groups where groupID = _groupID;
		delete from tbl_nwk_groupsToUsers where groupID = _groupID;
		delete from tbl_nwk_groupsToQuizzes where groupID = _groupID;

    delete from tbl_usr_preRegisterUsersToGroup where groupID = _groupID;
				
		call p_usr_recordActivity(_teacherUserID, 13, "", 6, _groupID);
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_getGroupMembers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_getGroupMembers`(in _userID bigint, in _userToken nvarchar(100), in _groupID bigint)
BEGIN
	declare lErrorCode int;
  set lErrorCode = 0;
    
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call p_nwk_getGroupMembers_internal(_userID, _groupID, lErrorCode);
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_getGroupMembers_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_getGroupMembers_internal`(in _teacherUserID bigint, in _groupID bigint, out _errorCode int)
BEGIN
	declare lTeacherID bigint;
  declare lGroupTypeID int;
  set _errorCode = 0;
    
  set lTeacherID = fn_usr_getTeacherIDByUserID(_teacherUserID);
        
  if(lTeacherID = 0) then
  	set _errorCode = -9; -- teacher not found
  elseif not exists(select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID) then
  	set _errorCode = -39; -- group doesn't belong to the teacher
  end if;
        
  if(_errorCode = 0) then
  	select 0 as returnValue, "ok" as errorText;
  	
    set lGroupTypeID = (select groupTypeID from tbl_nwk_groups where groupID = _groupID);
        
    if(lGroupTypeID = 1) then -- teacher's group
  		select t.userID, t.firstName, t.lastName, t.phoneNumber
  		from tbl_nwk_groups g
  			inner join tbl_nwk_groupsToUsers gu
  				on g.groupID = gu.groupID
  			inner join tbl_usr_teachers t
  				on gu.objectID = t.teacherID
  		where g.groupID = _groupID
              
      union all 
            
      select -1 as userID, "" as firstName, "" as lastName, phoneNumber
      from tbl_usr_preRegisterUsersToGroup
      where groupID = _groupID;
    else -- student's group
  		select s.userID, s.firstName, s.lastName, s.phoneNumber
  		from tbl_nwk_groups g
  			inner join tbl_nwk_groupsToUsers gu
  				on g.groupID = gu.groupID
  			inner join tbl_usr_students s
  				on gu.objectID = s.studentID
  		where g.groupID = _groupID
            
      union all 
            
      select -1 as userID, "" as firstName, "" as lastName, phoneNumber
      from tbl_usr_preRegisterUsersToGroup
      where groupID = _groupID;
    end if;
  else
  	select _errorCode as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_getTeacherGroups` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_getTeacherGroups`(in _userID bigint, in _userToken nvarchar(100), in _groupTypeID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
        if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		end if;
        
        if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
            			
			select g.groupID, groupTypeID, groupName, groupCode, count(gu.objectID) as usersInGroupCount
      from tbl_nwk_groups g
        left join tbl_nwk_groupsToUsers gu
          on g.groupID = gu.groupID
      where adminTeacherID = lTeacherID
        and (groupTypeID = _groupTypeID or _groupTypeID = 0)
      group by g.groupID;
        
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_removeMembersFromGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_removeMembersFromGroup`(in _userID bigint, in _userToken nvarchar(100), in _groupID bigint, in _phoneNumber varchar(45))
BEGIN
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    call p_nwk_removeMembersFromGroup_internal(_userID, _groupID, _phoneNumber);
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_removeMembersFromGroup_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_removeMembersFromGroup_internal`(in _teacherUserID bigint, in _groupID bigint, in _phoneNumber varchar(45))
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
  declare lUserID bigint;
  declare lFoundTeacherID bigint;
  declare lFoundStudentID bigint;
  set lErrorCode = 0;
  
		set lTeacherID = fn_usr_getTeacherIDByUserID(_teacherUserID);
        
    if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists(select 1 from tbl_nwk_groups where groupID = _groupID) then
			set lErrorCode = -45; -- group doesn't exist
		elseif not exists(select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID) then
			set lErrorCode = -39; -- group doesn't belong to the teacher
		end if;
        
    if(lErrorCode = 0) then
			set lUserID = fn_usr_getUserIDByPhoneNumber(_phoneNumber);
            
      if(lUserID > 0) then -- it is a registered user
				if(fn_usr_checkIfTeacherOrStudentByUserID(lUserID) = 1) then -- the user is a teacher
					set lFoundTeacherID = fn_usr_getTeacherIDByUserID(lUserID);
					
          if exists(select 1 from tbl_nwk_groupsToUsers gu
    								inner join tbl_nwk_groups g
    									on gu.groupID = g.groupID
    								where gu.groupID = _groupID and g.groupTypeID = 1 and gu.objectID = lFoundTeacherID) then
						delete gu.*
						from tbl_nwk_groupsToUsers gu
							inner join tbl_nwk_groups g
								on gu.groupID = g.groupID
							where gu.groupID = _groupID and g.groupTypeID = 1 and gu.objectID = lFoundTeacherID;
					else
						set lErrorCode = -51; -- user doesn't belong to the group
                    end if;
                else -- the user is a student
					set lFoundStudentID = fn_usr_getStudentIDByUserID(lUserID);
					
          if exists(select 1 from tbl_nwk_groupsToUsers gu
    								inner join tbl_nwk_groups g
    									on gu.groupID = g.groupID
    								where gu.groupID = _groupID and g.groupTypeID = 2 and gu.objectID = lFoundStudentID) then
						delete gu.*
						from tbl_nwk_groupsToUsers gu
							inner join tbl_nwk_groups g
								on gu.groupID = g.groupID
							where gu.groupID = _groupID and g.groupTypeID = 2 and gu.objectID = lFoundStudentID;
					else
						set lErrorCode = -51; -- user doesn't belong to the group
          end if;
        end if;
      else -- no user is found
				if exists(select 1 from tbl_usr_preRegisterUsersToGroup where phoneNumber = _phoneNumber and groupID = _groupID) then
					delete from tbl_usr_preRegisterUsersToGroup
					where phoneNumber = _phoneNumber
						and groupID = _groupID;
				else
					set lErrorCode = -51; -- user doesn't belong to the group
				end if;
            end if;
            
			if(lErrorCode = 0) then
				call p_usr_recordActivity(_teacherUserID, 19, "", 6, _groupID);
                
				select 0 as returnValue, "ok" as errorText;
            else
				select lErrorCode as returnValue, "error" as errorText;
			end if;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_shareQuizWithGroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_shareQuizWithGroup`(in _userID bigint, in _userToken nvarchar(100), in _groupID bigint, in _quizID bigint)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
  set lErrorCode = 0;
  
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		call p_nwk_shareQuizWithGroup_internal(_userID, _groupID, _quizID, lErrorCode);
        
    if(lErrorCode = 0) then
	    call p_usr_recordActivity(_userID, 15, "", 3, _quizID);

      call p_quiz_updateQuizStatistics(_quizID, 1);
            			
			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_shareQuizWithGroup_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_shareQuizWithGroup_internal`(in _teacherUserID bigint, in _groupID int, in _quizID bigint, out _errorCode int)
BEGIN
	declare lTeacherID bigint;
  set _errorCode = 0;
    
	set lTeacherID = fn_usr_getTeacherIDByUserID(_teacherUserID);
        
  if(lTeacherID = 0) then
  	set _errorCode = -9; -- teacher not found
  elseif not exists(select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID) then
  	set _errorCode = -39; -- group doesn't belong to the teacher
  elseif not exists(select 1 from tbl_quiz_quizzes where quizID = _quizID) then
  	set _errorCode = -18; -- quiz doesn't exist
  end if;
	
	if(_errorCode = 0) then
		if not exists(select 1 from tbl_nwk_groupsToQuizzes where groupID = _groupID and quizID = _quizID) then
  	  insert into tbl_nwk_groupsToQuizzes(groupID, quizID)
        select _groupID, _quizID;
  	end if;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_studentConnectToTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_studentConnectToTeacher`(in _userID bigint, in _userToken nvarchar(100), _groupCode varchar(10))
BEGIN
	declare lErrorCode int;
	declare lStudentID bigint;
	declare lGroupID bigint;
  set lErrorCode = 0;

  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    set lStudentID = fn_usr_getStudentIDByUserID(_userID);

    if(lStudentID = 0) then
			set lErrorCode = -53; -- user is not a student
    elseif not exists(select 1 from tbl_nwk_groups where groupCode = _groupCode) then
  	  set lErrorCode = -45; -- group doesn't exist
		elseif exists(select 1 from tbl_nwk_groupsToUsers where objectID = lStudentID) then
			set lErrorCode = -43; -- student can only belong to one group at a time
		end if;
        
    if(lErrorCode = 0) then
      set lGroupID = (select groupID from tbl_nwk_groups where groupCode = _groupCode);

		  insert into tbl_nwk_groupsToUsers
      select lGroupID, lStudentID;

			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_nwk_studentDisconnectFromTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_nwk_studentDisconnectFromTeacher`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	declare lErrorCode int;
	declare lStudentID bigint;
  set lErrorCode = 0;

  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
    set lStudentID = fn_usr_getStudentIDByUserID(_userID);

    if(lStudentID = 0) then
			set lErrorCode = -53; -- user is not a student
		elseif not exists(select 1 from tbl_nwk_groupsToUsers where objectID = lStudentID) then
			set lErrorCode = -68; -- student does not belong to any group
		end if;
        
    if(lErrorCode = 0) then
		  delete from tbl_nwk_groupsToUsers
      where objectID = lStudentID;

			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
    select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_cancelQuiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_cancelQuiz`(in _userID bigint, in _userToken nvarchar(100), in _questionID bigint)
BEGIN
  declare lQuizID bigint;
  
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		-- record the cancel quiz activity
    call p_usr_recordActivity(_userID, 8, "", 4, _questionID);

    set lQuizID = (select q.quizID
                   from tbl_quiz_quizQuestions qq
                    inner join tbl_quiz_quizzes q
                      on q.quizID = qq.quizID
                   where qq.questionID = _questionID);

    call p_quiz_updateQuizStatistics(lQuizID, 3);
        
		select 0 as returnValue, "ok" as errorText;
  else
		select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_copyQuestionsAndAnswers_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_copyQuestionsAndAnswers_internal`(in _sourceQuizID bigint, in _newQuizID bigint)
BEGIN
	declare currentQuestionID bigint;
  declare newQuestionID bigint;
  declare done int;
    
  declare cur1 cursor for select questionID from tbl_quiz_quizQuestions where quizID = _sourceQuizID;
	declare continue handler for not found set done=1;

  -- delete all existing questions & answers for the new quiz, if exists (this is for resaving quiz after deleting it)
  delete qa.*
	from tbl_quiz_quizQuestionsAnswers qa
		inner join tbl_quiz_quizQuestions qq 
			on qa.questionID = qq.questionID
	where qq.quizID = _newQuizID;

  delete from talkabout_db.tbl_quiz_quizQuestions where quizID = _newQuizID;

	set done = 0;
	open cur1;
		igmLoop: loop
			fetch cur1 into currentQuestionID;
			if done = 1 then leave igmLoop; end if;

			-- copy questions
			insert into tbl_quiz_quizQuestions(questionText, quizID, correctAnswerID, questionWordHints, correctAnswerAudioURL)
			select questionText, _newQuizID, correctAnswerID, questionWordHints, correctAnswerAudioURL
			from tbl_quiz_quizQuestions
			where quizID = _sourceQuizID
				and questionID = currentQuestionID;
			
			set newQuestionID = LAST_INSERT_ID();
				
			-- copy answers
			insert into tbl_quiz_quizQuestionsAnswers(questionID, answerID, answerText)
			select newQuestionID, answerID, answerText
            from tbl_quiz_quizQuestionsAnswers
			where questionID = currentQuestionID;
		end loop igmLoop;
	close cur1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_createEditAnswer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_createEditAnswer`(in _userID bigint, in _userToken nvarchar(100), in _questionID bigint, in _answerID int, in _answerText nvarchar(200), in _isLastItem int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from tbl_quiz_quizQuestions where questionID = _questionID) then
			set lErrorCode = -21; -- question doesn't exist
		elseif(CHAR_LENGTH(_answerText) <= 0) then
		 	set lErrorCode = -22; -- answer text cannot be empty
        end if;
        
        if(lErrorCode = 0) then
			if exists(select 1 from tbl_quiz_quizQuestionsAnswers where questionID = _questionID and answerID = _answerID) then -- answer exists, edit it
				if exists(select 1 from tbl_quiz_quizQuestionsAnswers where questionID = _questionID and answerID = _answerID and answerText != _answerText) then -- some data changed, update
					update tbl_quiz_quizQuestionsAnswers
                    set answerText = _answerText
                    where questionID = _questionID and answerID = _answerID;
                    
                    -- record the activity
                    call p_usr_recordActivity(_userID, 21, _answerText, 4, _questionID);
                end if;
            else -- insert a new answer
				insert into tbl_quiz_quizQuestionsAnswers(questionID, answerID, answerText)
				select _questionID, _answerID, _answerText;
                
                -- record the activity
				call p_usr_recordActivity(_userID, 22, _answerText, 4, _questionID);
			end if;
            
            if(_isLastItem > 0) then -- this is the last item, delete every answer remaining after this one
				delete from tbl_quiz_quizQuestionsAnswers
                where questionID = _questionID and answerID > _isLastItem; -- _isLastItem is the biggest answerID for this question
            end if;

			select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_createEditQuestion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_createEditQuestion`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint, in _questionID bigint, in _questionText nvarchar(200), in _correctAnswerID int, in _questionWordHints nvarchar(200))
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    declare lNewQuestionID bigint;
    set lErrorCode = 0;
    set lNewQuestionID = 0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
        if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists(select 1 from tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		elseif (not exists(select 1 from tbl_quiz_quizQuestions where questionID = _questionID)) and (_questionID > 0) then
			set lErrorCode = -21; -- question doesn't exist question
		elseif (not exists(select 1 from tbl_quiz_quizQuestions where quizID = _quizID and questionID = _questionID)) and (_questionID > 0) then
			set lErrorCode = -63; -- question doesn't belong to quiz
		elseif not exists(select 1 from tbl_quiz_quizzes where quizID = _quizID and teacherID = lTeacherID) then
			set lErrorCode = -56; -- teacher can only edit his own quizzes
		elseif(CHAR_LENGTH(_questionText) <= 0) then
			set lErrorCode = -19; -- question text cannot be empty
		elseif(_correctAnswerID <= 0) then
			set lErrorCode = -20; -- question must have a correct answer
        end if;
        
        if(lErrorCode = 0) then
			if(_questionID > 0) then -- edit existing question
				if exists(select 1 from tbl_quiz_quizQuestions where questionID = _questionID
																and (questionText != _questionText or
																	 correctAnswerID != _correctAnswerID or
                                                                     questionWordHints != _questionWordHints)) then -- some data changed, update
					update tbl_quiz_quizQuestions
					set questionText = _questionText,
						correctAnswerID = _correctAnswerID,
						questionWordHints = _questionWordHints
					where questionID = _questionID;
                    
                    call p_usr_recordActivity(_userID, 20, "", 4, _questionID);
				end if;
            
				set lNewQuestionID = _questionID;
            else -- insert a new question
				insert into tbl_quiz_quizQuestions(questionText, quizID, correctAnswerID, questionWordHints)
				select _questionText, _quizID, _correctAnswerID, _questionWordHints;
				
				set lNewQuestionID = LAST_INSERT_ID();
                
                call p_usr_recordActivity(_userID, 6, "", 4, lNewQuestionID);
            end if;
            
			select 0 as returnValue, "ok" as errorText;
            select lNewQuestionID as questionID;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_createQuizHierarchy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_createQuizHierarchy`(in _userID bigint, in _userToken nvarchar(100),
																 in _quizCategoryID bigint, in _quizCategoryName nvarchar(100), in _quizCategoryIcon varchar(100), in _alwaysShow int,
                                                                 in _quizSubjectID bigint, in _quizSubjectName nvarchar(100), in _quizSubjectIcon varchar(100), in _quizSubjectSubHeader varchar(100),
                                                                 in _quizName nvarchar(100), in _quizIcon varchar(100), in _quizTypeID int, in _quizFromPage int, in _quizToPage int)
BEGIN
	declare lTeacherID bigint;
	declare lErrorCode int;
    declare lCategoryID bigint;
    declare lSubjectID bigint;
    declare lNewQuizID bigint;
    set lErrorCode = 0;
    set lNewQuizID = 0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
        if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		end if;
		
		if(lErrorCode = 0) then
			-- ---------------------------------------handle category level--------------------------------------------------------------------------
            call p_quiz_createQuizHierarchy_internal(_userID, lTeacherID,
													 _quizCategoryID, _quizCategoryName, _quizCategoryIcon, _alwaysShow,
                                                     _quizSubjectID, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader,
                                                     _quizName, _quizIcon, _quizTypeID, _quizFromPage, _quizToPage,
                                                     lErrorCode, lCategoryID, lSubjectID, lNewQuizID);
            
            if(lErrorCode = 0) then
				select 0 as returnValue, "ok" as errorText;
				select lCategoryID as quizCategoryID, lSubjectID as quizSubjectID, lNewQuizID as quizID;
			else
				select lErrorCode as returnValue, "error" as errorText;
            end if;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_createQuizHierarchy_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_createQuizHierarchy_internal`(in _userID bigint, in _teacherID bigint,
																 in _quizCategoryID bigint, in _quizCategoryName nvarchar(100), in _quizCategoryIcon varchar(100), in _alwaysShow int,
                                                                 in _quizSubjectID bigint, in _quizSubjectName nvarchar(100), in _quizSubjectIcon varchar(100), in _quizSubjectSubHeader varchar(100),
                                                                 in _quizName nvarchar(100), in _quizIcon varchar(100), in _quizTypeID int, in _quizFromPage int, in _quizToPage int,
                                                                 out _errorCode int, out _chosenCategoryID bigint, out _chosenSubjectID bigint, out _newQuizID bigint)
BEGIN
    set _errorCode = 0;
    set _chosenCategoryID = -1;
    set _chosenSubjectID = 0;
    set _newQuizID = 0;
        
	if not exists(select 1 from tbl_quiz_quizTypes where quizTypeID = _quizTypeID) then -- check if quiz type exists
		set _errorCode = -15; -- quiz typeID doesn't exist
	elseif exists(select 1 from tbl_quiz_quizzes where quizSubjectID = _quizSubjectID and quizName = _quizName and quizTypeID = _quizTypeID) then
		set _errorCode = -14; -- quiz with same name and type already exists in the subject
	end if;
	
	if(_errorCode = 0) then
		if(CHAR_LENGTH(_quizCategoryIcon) = 0) then
			set _quizCategoryIcon = "classroom";
        end if;
        if(CHAR_LENGTH(_quizSubjectIcon) = 0) then
			set _quizSubjectIcon = "classroom";
        end if;
        if(CHAR_LENGTH(_quizIcon) = 0) then
			set _quizIcon = "classroom";
        end if;
    
		-- ---------------------------------------handle category level--------------------------------------------------------------------------
		if(_quizCategoryID = -1) then -- need to create new category or locate an existing one with the same name
			if exists (select 1 from tbl_quiz_categories where quizCategoryName = _quizCategoryName and teacherID = 0) then
				-- found a global category with the same name, return it
				set _chosenCategoryID = (select quizCategoryID from tbl_quiz_categories where quizCategoryName = _quizCategoryName and teacherID = 0);
			elseif exists (select 1 from tbl_quiz_categories where quizCategoryName = _quizCategoryName and teacherID = _teacherID) then
				-- found a category with the same name for this teacher, return it
				set _chosenCategoryID = (select quizCategoryID from tbl_quiz_categories where quizCategoryName = _quizCategoryName and teacherID = _teacherID);
			else
				-- category name was not found, create it under this teacher
				insert into tbl_quiz_categories(teacherID, quizCategoryName, quizCategoryIcon, alwaysShow)
				select _teacherID, _quizCategoryName, _quizCategoryIcon, _alwaysShow;
				
				set _chosenCategoryID = LAST_INSERT_ID();
				
				call p_usr_recordActivity(_userID, 3, "", 1, _chosenCategoryID);
			end if;
		else -- received a category to create underneath
			set _chosenCategoryID = _quizCategoryID;
		end if;
		
		-- ---------------------------------------handle subject level---------------------------------------------------------------------------
		if(_quizSubjectID = 0) then -- need to create new subject or locate an existing one with the same name
			if exists (select 1 from tbl_quiz_subjects where quizCategoryID = _chosenCategoryID and quizSubjectName = _quizSubjectName and teacherID = 0) then
				-- found a global subject with the same name, return it
				set _chosenSubjectID = (select quizSubjectID from tbl_quiz_subjects where quizCategoryID = _chosenCategoryID and quizSubjectName = _quizSubjectName and teacherID = 0);
			elseif exists (select 1 from tbl_quiz_subjects where quizCategoryID = _chosenCategoryID and quizSubjectName = _quizSubjectName and teacherID = _teacherID) then
				-- found a subject with the same name for this teacher, return it
				set _chosenSubjectID = (select quizSubjectID from tbl_quiz_subjects where quizCategoryID = _chosenCategoryID and quizSubjectName = _quizSubjectName and teacherID = _teacherID);
			else -- subject name was not found, create it under this teacher
        if(_chosenCategoryID = 0 and _teacherID = 0) then -- if the subject is a public book, use the icon from the category level
          set _quizSubjectIcon = (select quizCategoryIcon from tbl_quiz_categories where quizCategoryID = 0 and teacherID = 0);
        end if;

				insert into tbl_quiz_subjects(quizCategoryID, teacherID, quizSubjectName, quizSubjectIcon, quizSubjectSubHeader)
				select _chosenCategoryID, _teacherID, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader;
				
				set _chosenSubjectID = LAST_INSERT_ID();
				
				call p_usr_recordActivity(_userID, 4, "", 2, _chosenSubjectID);
			end if;
		else -- received a subject to create underneath
			set _chosenSubjectID = _quizSubjectID;
		end if;
		
		-- ---------------------------------------handle quiz level---------------------------------------------------------------------------
    if exists(select 1 from tbl_quiz_quizzes where quizSubjectID = _chosenSubjectID and quizName = _quizName and quizTypeID = _quizTypeID and teacherID = _teacherID) then
			if exists(select 1 from tbl_quiz_quizzes where quizSubjectID = _chosenSubjectID and quizName = _quizName and quizTypeID = _quizTypeID and teacherID = _teacherID and quizStatusTypeID = 0) then
        -- quiz with same parameters already exists, but is not active, need to reactivate it

        set _newQuizID = (select quizID from tbl_quiz_quizzes where quizSubjectID = _chosenSubjectID and quizName = _quizName and quizTypeID = _quizTypeID and teacherID = _teacherID and quizStatusTypeID = 0);
        
        update tbl_quiz_quizzes
          set quizStatusTypeID = 1
        where quizID = _newQuizID;

        call p_usr_recordActivity(_userID, 5, "", 3, _newQuizID);
      else
        set _errorCode = -14; -- quiz with same name and type already exists in the subject
      end if;
		else
			insert into tbl_quiz_quizzes(quizSubjectID, teacherID, quizName, quizTypeID, quizIcon, fromPage, toPage)
			select _chosenSubjectID, _teacherID, _quizName, _quizTypeID, _quizIcon, _quizFromPage, _quizToPage;
			
			set _newQuizID = LAST_INSERT_ID();
			
			call p_usr_recordActivity(_userID, 5, "", 3, _newQuizID);
		end if;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_deleteQuestion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_deleteQuestion`(in _userID bigint, in _userToken nvarchar(100), in _questionID bigint)
BEGIN

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		delete from tbl_quiz_quizQuestions where questionID = _questionID; -- delete the question
        
        delete from tbl_quiz_quizQuestionsAnswers where questionID = _questionID; -- delete the answers
        
        select 0 as returnValue, "ok" as errorText;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_deleteQuiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_deleteQuiz`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lTeacherID bigint;
	declare lErrorCode int;
    set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
        if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists (select 1 from tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		end if;
		
		if(lErrorCode = 0) then
			if exists(select 1 from tbl_quiz_quizzes where quizID = _quizID and teacherID = lTeacherID) then -- this is the teachers quiz
				update tbl_quiz_quizzes 
				set quizStatusTypeID = 0
				where quizID = _quizID and teacherID = lTeacherID;
				
				delete from tbl_nwk_groupsToQuizzes where quizID = _quizID;
            
				delete from tbl_nwk_teacherShareExceptions where quizID = _quizID;
				
				delete from tbl_usr_userAnswerSpeak where quizID = _quizID;
			
				delete from tbl_arcv_usr_userAnswerSpeak where quizID = _quizID;
				
				delete from tbl_rpt_userQuizSummary where quizID = _quizID;
				
				delete from tbl_arcv_rpt_userQuizSummary where quizID = _quizID;
				
				call p_usr_recordActivity(_userID, 10, "", 3, _quizID);
				
				select 0 as returnValue, "ok" as errorText;
            elseif exists(select 1 from tbl_quiz_quizzes where quizID = _quizID and teacherID = 0) then -- this is a public quiz
                -- delete only the sharing by the teacher
                delete gq.*
                from tbl_nwk_groupsToQuizzes gq
					inner join tbl_nwk_groups g
						on gq.groupID = g.groupID
				where g.adminTeacherID = lTeacherID;
                
                call p_usr_recordActivity(_userID, 14, "", 3, _quizID);
				
				select 0 as returnValue, "ok" as errorText;
            else -- the quiz belongs to another teacher
				select -40 as returnValue, "error" as errorText; -- teacher cannot delete other teachers quizzes
            end if;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_DeleteQuizQuestion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_DeleteQuizQuestion`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint, in _questionID bigint)
BEGIN
	declare lTeacherID bigint;
	declare lErrorCode int;
    set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
        if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists (select 1 from tbl_quiz_quizzes where quizID = _quizID and teacherID = lTeacherID) then
			set lErrorCode = -40; -- teacher can delete only his own quizzes
		elseif not exists (select 1 from tbl_quiz_quizQuestions where quizID = _quizID and questionID = _questionID) then
			set lErrorCode = -21; -- question doesn't exist
		end if;
		
		if(lErrorCode = 0) then
            call p_quiz_DeleteQuizQuestion_internal(_quizID, _questionID);
            
            call p_usr_recordActivity(_userID, 11, "", 4, _questionID);
            
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_DeleteQuizQuestion_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_DeleteQuizQuestion_internal`(in _quizID bigint, in _questionID bigint)
BEGIN
	delete qa.*
	from tbl_quiz_quizQuestionsAnswers qa
		inner join tbl_quiz_quizQuestions qq 
			on qa.questionID = qq.questionID
	where qq.quizID = _quizID
		 and qq.questionID = _questionID;
	
	delete from tbl_quiz_quizQuestions where quizID = _quizID and questionID = _questionID;
	
	delete from tbl_usr_userAnswerSpeak where quizID = _quizID and questionID = _questionID;
	
	delete from tbl_arcv_usr_userAnswerSpeak where quizID = _quizID and questionID = _questionID;
	
	-- recalc all quiz report (not including archive)
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_editCategory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_editCategory`(in _userID bigint, in _userToken nvarchar(100), in _quizCategoryID bigint, in _quizCategoryName nvarchar(100), in _quizCategoryIcon varchar(100), in _quizCategoryColor varchar(10), in _alwaysShow int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists(select 1 from talkabout_db.tbl_quiz_categories where quizCategoryID = _quizCategoryID) then
			set lErrorCode = -11; -- category doesn't exist
		else
			set lTeacherID = (select teacherID from talkabout_db.tbl_quiz_categories where quizCategoryID = _quizCategoryID);
			if exists(select 1 from talkabout_db.tbl_quiz_categories where quizCategoryName = _quizCategoryName and quizCategoryID <> _quizCategoryID and teacherID = lTeacherID) then
				set lErrorCode = -10; -- category with same name already exist for this teacher
            end if;
		end if;
        
        if(lErrorCode = 0) then
            update talkabout_db.tbl_quiz_categories
            set quizCategoryName = _quizCategoryName,
				quizCategoryIcon = _quizCategoryIcon,
                quizCategoryColor = _quizCategoryColor,
                alwaysShow = _alwaysShow
			where quizCategoryID = _quizCategoryID;
            
            -- record the activity
			call p_usr_recordActivity(_userID, 16, "", 1, _quizCategoryID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_editSubject` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_editSubject`(in _userID bigint, in _userToken nvarchar(100), in _quizSubjectID bigint, in _quizSubjectName nvarchar(100), in _quizSubjectIcon varchar(100), in _quizSubjectSubHeader varchar(100))
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    declare lQuizCategoryID bigint;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists(select 1 from talkabout_db.tbl_quiz_subjects where quizSubjectID = _quizSubjectID) then
			set lErrorCode = -13; -- subject doesn't exist
		else
			set lTeacherID = (select teacherID from talkabout_db.tbl_quiz_subjects where quizSubjectID = _quizSubjectID);
            set lQuizCategoryID = (select quizCategoryID from talkabout_db.tbl_quiz_subjects where quizSubjectID = _quizSubjectID);
			if exists(select 1 from talkabout_db.tbl_quiz_subjects where quizCategoryID = lQuizCategoryID and quizSubjectName = _quizSubjectName and teacherID = lTeacherID) then
				set lErrorCode = -12; -- subject with same name already exists in the category
            end if;
		end if;
        
        if(lErrorCode = 0) then
            update talkabout_db.tbl_quiz_subjects
            set quizSubjectName = _quizSubjectName,
				        quizSubjectIcon = _quizSubjectIcon,
                quizSubjectSubHeader = _quizSubjectSubHeader
			where quizSubjectID = _quizSubjectID;
            
            -- record the activity
			call p_usr_recordActivity(_userID, 17, "", 2, _quizSubjectID);
			
            select 0 as returnValue, "ok" as errorText;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getAllPublicSubjectsByCategory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getAllPublicSubjectsByCategory`(in _userID bigint, in _userToken nvarchar(100), in _quizCategoryID bigint)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
			select 0 as returnValue, "ok" as errorText;
			
      select quizSubjectID,
  				   quizSubjectName,
             quizSubjectSubHeader
			from talkabout_db.tbl_quiz_subjects
      where quizCategoryID = _quizCategoryID
        and teacherID = 0
			order by quizSubjectID;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getBooksQuizzesForTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getBooksQuizzesForTeacher`(in _userID bigint, in _userToken nvarchar(100), in _quizTypeID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
  		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
          
      if(lTeacherID = 0) then
  			set lErrorCode = -9; -- teacher not found
  		elseif not exists(select 1 from tbl_quiz_quizTypes where quizTypeID = _quizTypeID) then -- check if quiz type exists
  			set lErrorCode = -15; -- quiz typeID doesn't exist
      end if;
          
      if(lErrorCode = 0) then
  			select 0 as returnValue, "ok" as errorText;
  			
  			-- get general categories data
  			select distinct
    				   qs.quizSubjectID,
    				   qs.quizSubjectName,
    				   qs.quizSubjectIcon,
				       IFNULL(qs.quizSubjectSubHeader, "") as quizSubjectSubHeader,
    				   q.quizID,
               q.quizName,
               q.fromPage,
               q.toPage,
    				   q.quizTypeID,
    				   qs.quizSubjectIcon as quizIcon,
               q.teacherID,
               q.originalQuizID
  			from tbl_quiz_subjects qs
  				inner join tbl_quiz_quizzes q
  					on qs.quizSubjectID = q.quizSubjectID
  			where (q.teacherID = 0 or q.teacherID = lTeacherID)
  				and qs.quizCategoryID = 0 -- include only books category
  				and q.quizTypeID = _quizTypeID
                  and q.quizStatusTypeID = 1
  			order by qs.quizSubjectID, q.fromPage, q.toPage;
  		else
  			select lErrorCode as returnValue, "error" as errorText;
  		end if;
    else
		  select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getQuestionForComparison` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getQuestionForComparison`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint, in _questionID int)
BEGIN

	if(fn_usr_validateUserToken(_userID, _userToken) > 0 or talkabout_db_admin.fn_usr_validateUserToken(_userID, _userToken) > 0) then
		select 0 as returnValue, "ok" as errorText;
		
		-- get question correct answer
		select qa.answerID,
			   qa.answerText
		from tbl_quiz_quizQuestions qq
			inner join tbl_quiz_quizQuestionsAnswers qa
				on qq.questionID = qa.questionID
					and qq.correctAnswerID = qa.answerID
		where qq.quizID = _quizID
			and qq.questionID = _questionID;
		
		-- get question answers data
		select qa.answerID,
		   qa.answerText
		from tbl_quiz_quizQuestions qq
			inner join tbl_quiz_quizQuestionsAnswers qa
				on qq.questionID = qa.questionID
		where qq.quizID = _quizID
			and qq.questionID = _questionID;
            
		-- get the word hints
		select questionWordHints
        from tbl_quiz_quizQuestions
        where quizID = _quizID
			and questionID = _questionID;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getQuiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getQuiz`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
	
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
        end if;
        
        if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
			
			call p_quiz_getQuiz_internal(_quizID);
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getQuizzesForStudent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getQuizzesForStudent`(in _userID bigint, in _userToken nvarchar(100), in _quizTypeID int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from tbl_quiz_quizTypes where quizTypeID = _quizTypeID) then -- check if quiz type exists
			set lErrorCode = -15; -- quiz typeID doesn't exist
        end if;
        
        if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
			
			-- get general categories data
			select distinct
				   qc.quizCategoryID,
				   qc.quizCategoryName,
				   qc.quizCategoryIcon,
				   IFNULL(qc.quizCategoryColor, "") as quizCategoryColor,
           qc.alwaysShow,
				   qs.quizSubjectID,
				   qs.quizSubjectName,
				   qs.quizSubjectIcon,
				   IFNULL(qs.quizSubjectSubHeader, "") as quizSubjectSubHeader,
				   q.quizID,
           q.quizName,
           q.fromPage,
           q.toPage,
				   q.quizTypeID,
				   qs.quizSubjectIcon as quizIcon,
				   IFNULL(qsr.sequenceNumber, 0) as isEverCompleted
			from tbl_quiz_categories qc
				inner join tbl_quiz_subjects qs
					on qc.quizCategoryID = qs.quizCategoryID
						and qs.teacherID = 0
				inner join tbl_quiz_quizzes q
					on qs.quizSubjectID = q.quizSubjectID
						and q.teacherID = 0
				left join tbl_rpt_userQuizSummary qsr
					on q.quizID = qsr.quizID
						and qsr.userID = _userID
			where qc.teacherID = 0
				and q.quizTypeID = _quizTypeID
                and q.quizStatusTypeID = 1
			order by qc.quizCategoryID, qs.quizSubjectID, q.fromPage, q.toPage;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getQuizzesForTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getQuizzesForTeacher`(in _userID bigint, in _userToken nvarchar(100), in _quizTypeID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
        if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists(select 1 from tbl_quiz_quizTypes where quizTypeID = _quizTypeID) then -- check if quiz type exists
			set lErrorCode = -15; -- quiz typeID doesn't exist
        end if;
        
        if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
			
			-- get general categories data
			select distinct
				   qc.quizCategoryID,
				   qc.quizCategoryName,
				   qc.quizCategoryIcon,
				   IFNULL(qc.quizCategoryColor, "") as quizCategoryColor,
           qc.alwaysShow,
				   qs.quizSubjectID,
				   qs.quizSubjectName,
				   qs.quizSubjectIcon,
				   IFNULL(qs.quizSubjectSubHeader, "") as quizSubjectSubHeader,
				   q.quizID,
           q.quizName,
           q.fromPage,
           q.toPage,
				   q.quizTypeID,
				   qs.quizSubjectIcon As quizIcon,
           q.teacherID,
           q.originalQuizID
			from tbl_quiz_categories qc
				inner join tbl_quiz_subjects qs
					on qc.quizCategoryID = qs.quizCategoryID
						and qc.quizCategoryID > 0 -- exclude books category
				inner join tbl_quiz_quizzes q
					on qs.quizSubjectID = q.quizSubjectID
			where (q.teacherID = 0 or q.teacherID = lTeacherID)
				and q.quizTypeID = _quizTypeID
                and q.quizStatusTypeID = 1
			order by qc.quizCategoryID, qs.quizSubjectID, q.fromPage, q.toPage;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getQuizzesSharedWithTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getQuizzesSharedWithTeacher`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
  set lErrorCode = 0;
    
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
    if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		end if;
        
    if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
            
			select distinct 
				   q.quizID,
           q.quizName,
           q.fromPage,
           q.toPage,
				   q.quizTypeID,
				   qs.quizSubjectIcon As quizIcon
			from tbl_nwk_groupsToUsers gu
				inner join tbl_nwk_groupsToQuizzes gq
					on gu.groupID = gq.groupID
				inner join tbl_nwk_groups g
					on gu.groupID = g.groupID
						and g.groupTypeID = 1
				inner join tbl_quiz_quizzes q
					on q.quizID = gq.quizID
        inner join tbl_quiz_subjects qs
          on qs.quizSubjectID = q.quizSubjectID
				left join tbl_nwk_teacherShareExceptions se
					on se.quizID = q.quizID
						and se.teacherID = lTeacherID
			where gu.objectID = lTeacherID
				and q.quizStatusTypeID = 1
                and se.quizID is null;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getQuiz_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getQuiz_internal`(in _quizID bigint)
BEGIN
	-- get quiz data
	select q.quizID,
  		   q.quizName,
  		   qs.quizSubjectIcon as quizIcon,
  		   q.quizTypeID
	from tbl_quiz_quizzes q
    inner join tbl_quiz_subjects qs
      on q.quizSubjectID = qs.quizSubjectID
	where q.quizID = _quizID;
	
	-- get quiz questions data
	select qq.questionID,
		   qq.questionText,
		   qq.correctAnswerID
	from tbl_quiz_quizzes q
		inner join tbl_quiz_quizQuestions qq
			on q.quizID = qq.quizID
	where q.quizID = _quizID;
	
	-- get quiz answers data
	select qq.questionID,
		   qa.answerID,
		   qa.answerText
	from tbl_quiz_quizzes q
		inner join tbl_quiz_quizQuestions qq
			on q.quizID = qq.quizID
		inner join tbl_quiz_quizQuestionsAnswers qa
			on qq.questionID = qa.questionID
	where q.quizID = _quizID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getTeacherQuizzesForStudent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getTeacherQuizzesForStudent`(in _userID bigint, in _userToken nvarchar(100), in _quizTypeID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
    declare lStudentID bigint;
    set lErrorCode = 0;
    
    if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        if not exists(select 1 from tbl_quiz_quizTypes where quizTypeID = _quizTypeID) then
			set lErrorCode = -15; -- quiz typeID doesn't exist
        end if;
        
		if(lErrorCode = 0) then
			set lStudentID = fn_usr_getStudentIDByUserID(_userID);
			set lTeacherID = fn_usr_getTeacherIDByStudentID(lStudentID);
			
			select 0 as returnValue, "ok" as errorText;
			
			-- get the student's teacher quizzes
			select q.quizID,
           q.quizName,
           q.fromPage,
           q.toPage,
				   q.quizTypeID,
				   qs.quizSubjectIcon as quizIcon,
				   IFNULL(qsr.sequenceNumber, 0) as isEverCompleted
			from tbl_quiz_quizzes q
        inner join tbl_quiz_subjects qs
          on qs.quizSubjectID = q.quizSubjectID
				inner join tbl_nwk_groups g
					on (q.teacherID = g.adminTeacherID or q.teacherID = 0)
						and g.groupTypeID = 2 -- students groups
				inner join tbl_nwk_groupsToQuizzes gq
					on q.quizID = gq.quizID
						and g.groupID = gq.groupID
				inner join tbl_nwk_groupsToUsers gu
					on gq.groupID = gu.groupID
						and gu.objectID = lStudentID
				left join tbl_rpt_userQuizSummary qsr
					on q.quizID = qsr.quizID
						and qsr.userID = _userID
			where (q.teacherID = lTeacherID or q.teacherID = 0)
				and q.quizTypeID = _quizTypeID
                and q.quizStatusTypeID = 1;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_getTeacherQuizzesForTeacher` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_getTeacherQuizzesForTeacher`(in _userID bigint, in _userToken nvarchar(100), in _quizTypeID int)
BEGIN
	declare lErrorCode int;
	declare lTeacherID bigint;
  set lErrorCode = 0;
    
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
    if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists(select 1 from tbl_quiz_quizTypes where quizTypeID = _quizTypeID) then -- check if quiz type exists
			set lErrorCode = -15; -- quiz typeID doesn't exist
		end if;
        
    if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
			
      DROP TEMPORARY TABLE IF EXISTS tmp_teacherSharedQuizzes;
      
      CREATE TEMPORARY TABLE IF NOT EXISTS tmp_teacherSharedQuizzes AS 
      (
        select distinct 
				       q.quizID
  			from tbl_quiz_quizzes q
  				inner join tbl_nwk_groups g
  					on q.teacherID = g.adminTeacherID
  				inner join tbl_nwk_groupsToQuizzes gq
  					on q.quizID = gq.quizID
  						and g.groupID = gq.groupID
  			where q.teacherID = lTeacherID
  				and q.quizTypeID = _quizTypeID
          and q.quizStatusTypeID = 1
      );
            
            
      select *
      from 
      (
				-- get the teacher quizzes that are not shared
				select distinct
  					   q.quizID,
               q.quizName,
               q.fromPage,
               q.toPage,
  					   q.quizTypeID,
  					   qs.quizSubjectIcon as quizIcon,
  					   0 as isShared,
  					   lTeacherID as teacherID
				from tbl_quiz_quizzes q
          inner join tbl_quiz_subjects qs
            on qs.quizSubjectID = q.quizSubjectID
					left join tbl_nwk_groups g
						on q.teacherID = g.adminTeacherID
					left join tbl_nwk_groupsToQuizzes gq
						on q.quizID = gq.quizID
							and g.groupID = gq.groupID
					left join tmp_teacherSharedQuizzes tsq
						on tsq.quizID = q.quizID
				where q.teacherID = lTeacherID
					and q.quizTypeID = _quizTypeID
					and q.quizStatusTypeID = 1
					and gq.quizID is null
					and tsq.quizID is null
					
				union all
				
				-- get the teacher quizzes that are shared at least once
				select distinct 
  					   q.quizID,
               q.quizName,
               q.fromPage,
               q.toPage,
  					   q.quizTypeID,
  					   qs.quizSubjectIcon as quizIcon,
  					   IFNULL(gq.quizID, 0) as isShared,
  					   lTeacherID as teacherID
				from tbl_quiz_quizzes q
          inner join tbl_quiz_subjects qs
            on qs.quizSubjectID = q.quizSubjectID
					inner join tbl_nwk_groups g
						on q.teacherID = g.adminTeacherID
					inner join tbl_nwk_groupsToQuizzes gq
						on q.quizID = gq.quizID
							and g.groupID = gq.groupID
				where q.teacherID = lTeacherID
					and q.quizTypeID = _quizTypeID
					and q.quizStatusTypeID = 1
					
				union all
				
				-- get the public quizzes assigned by the teacher
				select distinct
  					   q.quizID,
               q.quizName,
               q.fromPage,
               q.toPage,
  					   q.quizTypeID,
  					   qs.quizSubjectIcon as quizIcon,
  					   IFNULL(gq.quizID, 0) as isShared,
  					   0 as teacherID
				from tbl_nwk_groups g
					inner join tbl_nwk_groupsToQuizzes gq
						on g.groupID = gq.groupID
              and g.adminTeacherID = lTeacherID
					inner join tbl_quiz_quizzes q
						on gq.quizID = q.quizID
          inner join tbl_quiz_subjects qs
            on qs.quizSubjectID = q.quizSubjectID
				where q.teacherID = 0
					and q.quizTypeID = _quizTypeID
					and q.quizStatusTypeID = 1
			) a
		order by quizID;
                
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_saveQuizCopy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_saveQuizCopy`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lTeacherID bigint;
	declare lErrorCode int;
  declare _quizCategoryID bigint;
  declare _quizCategoryName nvarchar(100);
  declare _quizCategoryIcon varchar(100);
  declare _quizSubjectID bigint;
  declare _quizSubjectName nvarchar(100);
  declare _quizSubjectIcon varchar(100);
  declare _quizSubjectSubHeader varchar(100);
  declare _quizName nvarchar(100);
  declare _quizIcon varchar(100);
  declare _quizTypeID int;
  declare _quizFromPage int;
  declare _quizToPage int;
  declare lCategoryID bigint;
  declare lSubjectID bigint;
  declare lNewQuizID bigint;
  
  set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
    -- TODO
    if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists (select 1 from tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		end if;
		
		if(lErrorCode = 0) then
			
      select c.quizCategoryID, c.quizCategoryName, c.quizCategoryIcon,
		         s.quizSubjectID, s.quizSubjectName, s.quizSubjectIcon, s.quizSubjectSubHeader,
             q.quizName, q.quizIcon, q.quizTypeID, q.fromPage, q.toPage
			into _quizCategoryID, _quizCategoryName, _quizCategoryIcon,
				   _quizSubjectID, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader,
				   _quizName, _quizIcon, _quizTypeID, _quizFromPage, _quizToPage
      from tbl_quiz_categories c
				inner join tbl_quiz_subjects s
					on c.quizCategoryID = s.quizCategoryID
				inner join tbl_quiz_quizzes q
					on q.quizSubjectID = s.quizSubjectID
			where q.quizID = _quizID;
        
			-- create the new quiz for the requesting teacher, if needed it will create also the category and the subject
			call p_quiz_createQuizHierarchy_internal(_userID, lTeacherID,
													                     -1, _quizCategoryName, _quizCategoryIcon,
                                                0, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader,
                                                _quizName, _quizIcon, _quizTypeID, _quizFromPage, _quizToPage,
                                                lErrorCode, lCategoryID, lSubjectID, lNewQuizID);
            
      if(lErrorCode = 0) then
				-- update original quizID
        update tbl_quiz_quizzes
        set originalQuizID = _quizID
        where quizID = lNewQuizID;
        
        call p_quiz_copyQuestionsAndAnswers_internal(_quizID, lNewQuizID);
        
        call p_usr_recordActivity(_userID, 17, "", 3, _quizID);

        call p_quiz_updateQuizStatistics(_quizID, 2);
            
				select 0 as returnValue, "ok" as errorText;
				select lCategoryID as quizCategoryID, lSubjectID as quizSubjectID, lNewQuizID as quizID;
			else
				select lErrorCode as returnValue, "error" as errorText;
      end if;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_saveQuizCopyAs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_saveQuizCopyAs`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint, in _newQuizName nvarchar(100))
BEGIN
	declare lErrorCode int;
  declare _quizCategoryID bigint;
  declare _quizCategoryName nvarchar(100);
  declare _quizCategoryIcon varchar(100);
  declare _alwaysShow int;
  declare _quizSubjectID bigint;
  declare _quizSubjectName nvarchar(100);
  declare _quizSubjectIcon varchar(100);
  declare _quizSubjectSubHeader varchar(100);
  declare _quizIcon varchar(100);
  declare _quizTypeID int;
  declare _quizFromPage int;
  declare _quizToPage int;
  declare lCategoryID bigint;
  declare lSubjectID bigint;
  declare lNewQuizID bigint;
  
  set lErrorCode = 0;

	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
        
    if not exists (select 1 from talkabout_db.tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		end if;
		
		if(lErrorCode = 0) then
			
      select c.quizCategoryID, c.quizCategoryName, c.quizCategoryIcon, c.alwaysShow,
		         s.quizSubjectID, s.quizSubjectName, s.quizSubjectIcon, s.quizSubjectSubHeader,
             q.quizIcon, q.quizTypeID, q.fromPage, q.toPage
			into _quizCategoryID, _quizCategoryName, _quizCategoryIcon, _alwaysShow,
				   _quizSubjectID, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader,
				   _quizIcon, _quizTypeID, _quizFromPage, _quizToPage
      from talkabout_db.tbl_quiz_categories c
				inner join talkabout_db.tbl_quiz_subjects s
					on c.quizCategoryID = s.quizCategoryID
				inner join talkabout_db.tbl_quiz_quizzes q
					on q.quizSubjectID = s.quizSubjectID
			where q.quizID = _quizID;
        
			-- create the new quiz for the requesting teacher, if needed it will create also the category and the subject
			call talkabout_db.p_quiz_createQuizHierarchy_internal(0, 0,
              													                     -1, _quizCategoryName, _quizCategoryIcon, _alwaysShow,
                                                              0, _quizSubjectName, _quizSubjectIcon, _quizSubjectSubHeader,
                                                              _newQuizName, _quizIcon, _quizTypeID, _quizFromPage, _quizToPage,
                                                              lErrorCode, lCategoryID, lSubjectID, lNewQuizID);
            
      if(lErrorCode = 0) then
				-- update original quizID
        update talkabout_db.tbl_quiz_quizzes
        set originalQuizID = _quizID,
            quizStatusTypeID = 0
        where quizID = lNewQuizID;
        
        call talkabout_db.p_quiz_copyQuestionsAndAnswers_internal(_quizID, lNewQuizID);
        
        call p_usr_recordActivity(_userID, 31, "", 3, _quizID);

        call talkabout_db.p_quiz_updateQuizStatistics(_quizID, 2);
            
				select 0 as returnValue, "ok" as errorText;
				select lCategoryID as quizCategoryID, lSubjectID as quizSubjectID, lNewQuizID as quizID;
			else
				select lErrorCode as returnValue, "error" as errorText;
      end if;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_skipQuestion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_skipQuestion`(in _userID bigint, in _userToken nvarchar(100), in _questionID bigint)
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		-- record the cancel quiz activity
        call p_usr_recordActivity(_userID, 23, "", 4, _questionID);
        
		select 0 as returnValue, "ok" as errorText;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_quiz_updateQuizStatistics` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_quiz_updateQuizStatistics`(in _quizID bigint, in _statType int)
BEGIN
  if not exists (select 1 from tbl_quiz_quizStatistics where quizID = _quizID) then
    insert into tbl_quiz_quizStatistics(quizID)
    select _quizID;
  end if;

  if(_statType = 1) then -- share quiz
    update tbl_quiz_quizStatistics
    set shareCount = shareCount + 1
    where quizID = _quizID;
  elseif (_statType = 2) then -- save quiz
    update tbl_quiz_quizStatistics
    set saveSourceCount = saveSourceCount + 1
    where quizID = _quizID;
  elseif (_statType = 3) then -- cancel quiz
    update tbl_quiz_quizStatistics
    set cancelCount = cancelCount + 1
    where quizID = _quizID;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_rpt_getDetailedQuizReport` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_rpt_getDetailedQuizReport`(IN _userID bigint, IN _userToken nvarchar(100), IN _groupID bigint, IN _quizID bigint)
BEGIN
	declare lErrorCode int;
    declare lTeacherID bigint;
    set lErrorCode = 0;
    
    SET @rank=0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
    if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists(select 1 from tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		elseif not exists (select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID) then
			set lErrorCode = -39; -- group doesn't belong to the teacher
		elseif not exists (select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID and groupTypeID = 2) then
			set lErrorCode = -66; -- reports are available only for student groups
		end if;
        
    if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;

      -- get number of students per range by finalScore
      select CONCAT(cast(rr.`from` as char(10)), "-", cast(rr.`to` as char(10))) as `range`, ifnull(count(s.studentID), 0) as rangeCount
      from tbl_rpt_reportRanges rr
				left join tbl_rpt_userQuizSummary qs
          on qs.finalScoreAvg > rr.`from`
            and qs.finalScoreAvg <= rr.`to`
            and qs.quizID = _quizID
        left join tbl_nwk_groupsToUsers gu
          on gu.groupID = _groupID
        left join tbl_usr_students s
          on gu.objectID = s.studentID
            and s.userID = qs.userID
      group by rr.`from`, rr.`to`;

      -- get how many students answer correctly per question
      select gq.groupID, gq.quizID, uas.questionID, count(1) as totalCount, count(qq.questionID) as subCount
			from tbl_nwk_groupsToQuizzes gq
				inner join tbl_usr_userAnswerSpeak uas
					on gq.quizID = uas.quizID
        inner join tbl_nwk_groupsToUsers gu
          on gq.groupID = gu.groupID
        inner join tbl_usr_students s
          on s.studentID = gu.objectID
            and s.userID = uas.userID
				inner join tbl_rpt_userQuizSummary qs
					on uas.sequenceNumber = qs.sequenceNumber
				left join tbl_quiz_quizQuestions qq
					on uas.quizID = qq.quizID
						and uas.questionID = qq.questionID
						and uas.recognizedAnswerID = qq.correctAnswerID
			where gq.groupID = _groupID
        and gq.quizID = _quizID
				group by gq.groupID, gq.quizID, uas.questionID;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
  select -8 as returnValue, "error" as errorText; -- invalid token
  end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_rpt_getDetailedQuizReportByStudent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_rpt_getDetailedQuizReportByStudent`(IN _userID bigint, IN _userToken nvarchar(100), IN _groupID bigint, IN _quizID bigint)
BEGIN
	declare lErrorCode int;
    declare lTeacherID bigint;
    set lErrorCode = 0;
    
    SET @rank=0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
        if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists(select 1 from tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		elseif not exists (select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID) then
			set lErrorCode = -39; -- group doesn't belong to the teacher
		elseif not exists (select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID and groupTypeID = 2) then
			set lErrorCode = -66; -- reports are available only for student groups
		end if;
        
        if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;

			SELECT @rn:=@rn+1 AS rank, firstName, lastName, userID, quizID, sequenceNumber, correctAnswersCount, audioConfidenceAvg, finalScoreAvg
			FROM (
				select s.firstName, s.lastName, qs.userID, qs.quizID, qs.sequenceNumber, qs.correctAnswersCount, qs.audioConfidenceAvg, qs.finalScoreAvg
				from  tbl_rpt_userQuizSummary qs
					inner join tbl_usr_students s
						on qs.userID = s.userID
					inner join tbl_nwk_groupsToUsers gu
						on s.studentID = gu.objectID
				where gu.groupID = _groupID
					and qs.quizID = _quizID
				order by correctAnswersCount desc, finalScoreAvg desc
			) t1, (SELECT @rn:=0) t2;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_rpt_getGroupQuizzesReports` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_rpt_getGroupQuizzesReports`(in _userID bigint, in _userToken nvarchar(100), in _groupID bigint)
BEGIN
	declare lErrorCode int;
    declare lTeacherID bigint;
    set lErrorCode = 0;
    
    SET @rank=0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherID = fn_usr_getTeacherIDByUserID(_userID);
        
    if(lTeacherID = 0) then
			set lErrorCode = -9; -- teacher not found
		elseif not exists (select 1 from tbl_nwk_groups where groupID = _groupID and adminTeacherID = lTeacherID and groupTypeID = 2) then
			set lErrorCode = -39; -- group doesn't belong to the teacher
		end if;
        
    if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;

			-- get main statistics per group per quiz
			select distinct
		         gq.groupID,
             gq.quizID,
             q.quizName,
             q.fromPage,
             q.toPage,
             q.quizIcon,
				     IFNULL(avg(qs.correctAnswersCount), 0) as correctAnswersCount,
             IFNULL(avg(qs.audioConfidenceAvg), 0) as audioConfidenceAvg, 
             IFNULL(avg(qs.finalScoreAvg), 0) as finalScoreAvg, 
             count(s.userID) as totalCount, 
             count(qs.userID) as subCount
      from tbl_nwk_groupsToQuizzes gq
				inner join tbl_quiz_quizzes q
					on gq.quizID = q.quizID
				inner join tbl_nwk_groupsToUsers gu
					on gq.groupID = gu.groupID
				inner join tbl_usr_students s
					on s.studentID = gu.objectID
				left join tbl_rpt_userQuizSummary qs
					on qs.userID = s.userID
						and qs.quizID = gq.quizID
			where gq.groupID = _groupID
            group by gq.groupID, gq.quizID;

      -- get questions count per group per quiz
			select distinct
		         gq.groupID,
             gq.quizID,
             count(qq.questionID) as questionsCount
      from tbl_nwk_groupsToQuizzes gq
				inner join tbl_quiz_quizzes q
					on gq.quizID = q.quizID
        inner join tbl_quiz_quizQuestions qq
          on q.quizID = qq.quizID
			where gq.groupID = _groupID
            group by gq.groupID, gq.quizID;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
  else
		select -8 as returnValue, "error" as errorText; -- invalid token
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_rpt_getStudentQuizReport` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_rpt_getStudentQuizReport`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint)
BEGIN
	declare lErrorCode int;
    declare lGroupID bigint;
    declare lStudentID bigint;
    declare lGroupCount int;
    set lErrorCode = 0;
    set lGroupID = 0;
    set lGroupCount = 0;
    
    SET @rank=0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lStudentID = fn_usr_getStudentIDByUserID(_userID);
        
		if not exists(select 1 from tbl_quiz_quizzes where quizID = _quizID) then
			set lErrorCode = -18; -- quiz doesn't exist
		elseif (lStudentID <= 0) then
			set lErrorCode = -36; -- student not found
		elseif not exists(select 1 from  tbl_rpt_userQuizSummary where userID = _userID	and quizID = _quizID) then
			set lErrorCode = -37; -- quiz report not found for user and quiz
        end if;
        
        if(lErrorCode = 0) then
			select 0 as returnValue, "ok" as errorText;
            
			if exists(select 1 from tbl_nwk_groupsToUsers gu
						inner join tbl_nwk_groups g
							on gu.groupID = g.groupID
						where objectID = lStudentID and g.groupTypeID = 2) then -- student belongs to a group
				-- find the groupID
                set lGroupID = (select g.groupID from tbl_nwk_groupsToUsers gu
								inner join tbl_nwk_groups g
									on gu.groupID = g.groupID
								where objectID = lStudentID and g.groupTypeID = 2);
            
				select rank, userID, correctAnswersCount, audioConfidenceAvg, finalScoreAvg
				from (
					SELECT @rn:=@rn+1 AS rank, userID, correctAnswersCount, audioConfidenceAvg, finalScoreAvg
					FROM (
						select qs.userID, qs.correctAnswersCount, qs.audioConfidenceAvg, qs.finalScoreAvg
						from  tbl_rpt_userQuizSummary qs
							inner join tbl_usr_students s
								on qs.userID = s.userID
							inner join tbl_nwk_groupsToUsers gu
								on s.studentID = gu.objectID
						where gu.groupID = lGroupID
							and qs.quizID = _quizID
						order by correctAnswersCount desc, finalScoreAvg desc
					) t1, (SELECT @rn:=0) t2
				) t3
				where userID = _userID;
                
                if exists(select 1 from tbl_nwk_groupsToQuizzes where groupID = lGroupID and quizID = _quizID) then
					set lGroupCount = (select count(1) from tbl_nwk_groupsToUsers where groupID = lGroupID);
					select lGroupCount as groupCount;
				else -- the quiz is not assigned to the group
					select -1 as groupCount; -- indicates not to show the position graph
                end if;
			else -- student doesn't belong to a group
				select 1 as rank, userID, correctAnswersCount, audioConfidenceAvg, finalScoreAvg
				from  tbl_rpt_userQuizSummary
                where userID = _userID
					and quizID = _quizID;
                    
				select -1 as groupCount; -- indicates not to show the position graph
            end if;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_createVerificationCode` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_createVerificationCode`(in _userPhoneNumber varchar(45), in _verificationTypeID int)
BEGIN
	declare newTempCode int;
    set newTempCode = FLOOR(RAND() * 9000) + 1000; -- random number between 1000 and 9999

	if exists (select 1 from tbl_usr_tempVerificationCodes where phoneNumber = _userPhoneNumber and verificationTypeID = _verificationTypeID) then
		update tbl_usr_tempVerificationCodes
        set tempCode = newTempCode,
			lastUpdate = current_timestamp
        where phoneNumber = _userPhoneNumber 
			and verificationTypeID = _verificationTypeID;
    else
		insert into tbl_usr_tempVerificationCodes(phoneNumber, verificationTypeID, tempCode)
        select _userPhoneNumber, _verificationTypeID, newTempCode;
    end if;
    
    select 0 as returnValue, "ok" as errorText;
    select newTempCode as verificationCode;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_deleteUserAnswersBySequence` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_deleteUserAnswersBySequence`(in _userID bigint, in _userToken nvarchar(100), in _sequenceNumber bigint)
BEGIN
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		delete from tbl_usr_userAnswerSpeak
		where userID = _userID
			and sequenceNumber = _sequenceNumber;
		
		select 0 as returnValue, "ok" as errorText;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_getUserData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_getUserData`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	declare lTeacherOrStudent int;
  declare lStudentID bigint;
  declare lStudentTeacherID bigint;
  set lStudentID = 0;
  set lStudentTeacherID = 0;
    
  if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		set lTeacherOrStudent = fn_usr_checkIfTeacherOrStudentByUserID(_userID);
    
		if(lTeacherOrStudent = 0) then -- user not found
			select -1 as returnValue, "error" as errorText; -- user not found
    else
			select 0 as returnValue, "ok" as errorText;
            
      if(lTeacherOrStudent = 1) then -- user is a teacher
				select u.userID, u.userTypeID, u.statusTypeID, t.teacherID, t.firstName, t.lastName, t.birthYear, t.gender, t.phoneNumber
				from tbl_usr_users u
					inner join tbl_usr_teachers t
						on u.userID = t.userID
				where u.userID = _userID;

        select 0 as studentTeacherID;

        select "" as studentTeacherFirstName, "" as studentTeacherLastName;
      else -- user is a student
				select u.userID, u.userTypeID, u.statusTypeID, s.studentID, s.firstName, s.lastName, s.birthYear, s.gender, s.phoneNumber
				from tbl_usr_users u
					inner join tbl_usr_students s
						on u.userID = s.userID
				where u.userID = _userID;

        set lStudentID = (select s.studentID from tbl_usr_users u
                                    					inner join tbl_usr_students s
                                    						on u.userID = s.userID where u.userID = _userID);

        if exists (select 1 from tbl_nwk_groupsToUsers where objectID = lStudentID) then
          set lStudentTeacherID = (select g.adminTeacherID from tbl_nwk_groupsToUsers gu
                                                  					inner join tbl_nwk_groups g
                                                  						on g.groupID = gu.groupID where gu.objectID = lStudentID LIMIT 1);
        end if;

        select lStudentTeacherID as studentTeacherID;

        if(lStudentTeacherID > 0) then
          select firstName as studentTeacherFirstName, lastName as studentTeacherLastName from tbl_usr_teachers where teacherID = lStudentTeacherID;
        else
          select "" as studentTeacherFirstName, "" as studentTeacherLastName;
        end if;
      end if;
    end if;
  else
		select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_login`(in _userPhoneNumber varchar(45), in _userPassword varchar(100))
BEGIN
	declare currentUserID bigint;
	declare newToken varchar(100);
    set currentUserID = 0;
    
    set currentUserID = fn_usr_getUserIDByPhoneNumber(_userPhoneNumber);
    
	if exists (select 1 from tbl_usr_users where userID = currentUserID and userPassword = _userPassword) then
		set newToken = UUID();
        
        if exists (select 1 from tbl_usr_userTokens where userID = currentUserID) then
			update tbl_usr_userTokens
            set userToken = newToken
            where userID = currentUserID;
        else
			insert into tbl_usr_userTokens(userID, userToken)
            select currentUserID, newToken;
        end if;
        
        -- record the login activity
        call p_usr_recordActivity(currentUserID, 1, "", 0, 0);
        
        select 0 as returnValue, "ok" as errorText;
        select currentUserID as userID, newToken as userToken;
    else
		select -28 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_logout` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_logout`(in _userID bigint, in _userToken nvarchar(100))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		delete from tbl_usr_userTokens
        where userID = _userID;
            
		-- record the logout activity
        call p_usr_recordActivity(_userID, 2, "", 0, 0);
        
		select 0 as returnValue, "ok" as errorText;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_recordActivity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_recordActivity`(in _userID bigint, in _activityTypeID int, in _freeText varchar(500), in _objectTypeID int, in _objectID bigint)
BEGIN
	-- record the user activity
	insert into tbl_usr_usersActivity(userID, activityTypeID, freeText, objectTypeID, objectID)
	select _userID, _activityTypeID, _freeText, _objectTypeID, _objectID;
    
    -- update the last update time of the main token
    update tbl_usr_userTokens
    set lastUpdate = now()
    where userID = _userID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_registerUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_registerUser`(in _firstName nvarchar(50), in _lastName nvarchar(50), in _password varchar(100),
                                          in _phoneNumber varchar(45), in _gender int, in _birthYear int, in _groupCode varchar(10))
BEGIN
	declare lErrorCode int;
  declare lNewUserID bigint;  
  declare lNewToken varchar(100);
  set lErrorCode = 0;
	set lNewUserID = 0;
	
  call p_usr_registerUser_internal(_firstName, _lastName, _password, _phoneNumber, _gender, _birthYear, _groupCode, lErrorCode, lNewUserID, lNewToken);
  if(lErrorCode = 0) then
    select lErrorCode as returnValue, "ok" as errorText;
    select lNewUserID as userID, lNewToken as userToken;
  else
    select lErrorCode as returnValue, "error" as errorText; 
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_registerUserCheckAvailability` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_registerUserCheckAvailability`(in userFirstName nvarchar(50),
 in userLastName nvarchar(50),
 in userPassword varchar(100),
 in userPhoneNumber varchar(45),
 in userGender int,
 in userBirthYear int)
BEGIN
	declare returnValue int;
	set returnValue = 0;
	
    if(CHAR_LENGTH(userFirstName) > 0 and CHAR_LENGTH(userLastName) > 0 and CHAR_LENGTH(userPassword) > 0 and CHAR_LENGTH(userPhoneNumber) > 0 and userBirthYear > 0) then
		if exists (select 1 from tbl_usr_genderTypes where genderTypeID = userGender) then
			if exists (select 1 from tbl_usr_students where phoneNumber = userPhoneNumber) then
				set returnValue = -5; -- number already exists
			elseif exists (select 1 from tbl_usr_teachers where phoneNumber = userPhoneNumber) then
				set returnValue = -5; -- number already exists
			end if;
		else
			set returnValue = -4; -- gender doesn't exist
		end if;
    else
		set returnValue = -2; -- missing parameters
    end if;
    
    if returnValue < 0 then
		select returnValue, "error" as errorText;
    else
		select returnValue, "ok" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_registerUser_internal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_registerUser_internal`(in _firstName nvarchar(50), in _lastName nvarchar(50), in _password varchar(100),
                                                   in _phoneNumber varchar(45), in _gender int, in _birthYear int, in _groupCode varchar(10),
                                                   out _errorCode int, out _newUserID bigint, out _newToken varchar(100))
BEGIN
  declare lUserTypeID TINYINT(1);
  declare lNewObjectID bigint;
  declare lGroupID bigint;
  set _errorCode = 0;
  set _newUserID = 0;
	set lUserTypeID = 0;
	
  if(CHAR_LENGTH(_firstName) <= 0 or CHAR_LENGTH(_lastName) <= 0 or CHAR_LENGTH(_password) <= 0 or CHAR_LENGTH(_phoneNumber) <= 0 or _birthYear <= 0) then
    set _errorCode = -2; -- missing parameters
  elseif not exists (select 1 from tbl_usr_genderTypes where genderTypeID = _gender) then
    set _errorCode = -4; -- gender doesn't exist
	elseif exists (select 1 from tbl_usr_students where phoneNumber = _phoneNumber) then
		set _errorCode = -5; -- number already exists
	elseif exists (select 1 from tbl_usr_teachers where phoneNumber = _phoneNumber) then
		set _errorCode = -5; -- number already exists
  end if;
    
  if(_errorCode = 0) then
		if exists(select 1 from tbl_usr_preRegisterTeachersPhoneNumbers where phoneNumber = _phoneNumber) then -- this user is a teacher
			set lUserTypeID = 1;
        else -- this user is a student
			set lUserTypeID = 2;
    end if;
        
    -- create the user
		insert into tbl_usr_users (userPassword, userTypeID, statusTypeID)
		select _password, lUserTypeID, 1;
		
		set _newUserID = LAST_INSERT_ID(); -- ID of new user created
		
    if (lUserTypeID = 1) then -- this user is a teacher
			insert into tbl_usr_teachers(userID, firstName, lastName, birthYear, gender, phoneNumber)
      select _newUserID, _firstName, _lastName, _birthYear, _gender, _phoneNumber;
            
      set lNewObjectID = LAST_INSERT_ID(); -- ID of new teacher created
    else -- this user is a student
			insert into tbl_usr_students(userID, firstName, lastName, birthYear, gender, phoneNumber)
      select _newUserID, _firstName, _lastName, _birthYear, _gender, _phoneNumber;
            
      set lNewObjectID = LAST_INSERT_ID(); -- ID of new student created
    end if;
        
    -- check if the phone number is pre registered fot a group
    if exists(select 1 from tbl_usr_preRegisterUsersToGroup where phoneNumber = _phoneNumber) then
			insert into tbl_nwk_groupsToUsers(groupID, objectID)
			select groupID, lNewObjectID
			from tbl_usr_preRegisterUsersToGroup
			where phoneNumber = _phoneNumber;
            
      delete from tbl_usr_preRegisterUsersToGroup where phoneNumber = _phoneNumber;
    elseif (lUserTypeID = 2 AND CHAR_LENGTH(_groupCode) > 0) THEN -- if student and groupCode exists - register to the relevant group
      if exists(select 1 from tbl_nwk_groups where groupCode = _groupCode) then
        set lGroupID = (select groupID from tbl_nwk_groups where groupCode = _groupCode);
  
  		  insert into tbl_nwk_groupsToUsers
        select lGroupID, lNewObjectID;
      end if;
    end if;
        
		-- deletes the verification code that was created for this user registration
		delete from tbl_usr_tempVerificationCodes
		where phonenumber = _phoneNumber
			and verificationtypeid = 1;

		-- create new token to avoid login process right after register
		set _newToken = UUID();
        
		if exists (select 1 from tbl_usr_userTokens where userID = _newUserID) then
			update tbl_usr_userTokens
      set userToken = _newToken
      where userID = _newUserID;
    else
			insert into tbl_usr_userTokens(userID, userToken)
      select _newUserID, _newToken;
    end if;
        
    -- record the login activity
    call p_usr_recordActivity(_newUserID, 1, "after register", 0, 0);
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_reportQuestionBug` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_reportQuestionBug`(in _userID bigint, in _userToken nvarchar(100), in _questionID bigint, in _bugText nvarchar(500))
BEGIN
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		-- record the question bug activity
        call p_usr_recordActivity(_userID, 9, _bugText, 4, _questionID);
        
		select 0 as returnValue, "ok" as errorText;
    else
		select -8 as returnValue, "error" as errorText;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_submitUserAnswer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_submitUserAnswer`(in _userID bigint, in _userToken nvarchar(100), in _quizID bigint, in _questionID bigint, in _recognizedAnswerID int, in _audioTranscript nvarchar(100), in _audioConfidence double(4,2), in _finalScore double(4,2), in _sequenceNumber bigint, in _isLastItem int)
BEGIN
	declare lErrorCode int;
    set lErrorCode = 0;
    
	if(fn_usr_validateUserToken(_userID, _userToken) > 0) then
		if not exists(select 1 from tbl_quiz_quizQuestions where questionID = _questionID) then
			set lErrorCode = -21; -- question doesn't exist
		elseif(CHAR_LENGTH(_audioTranscript) <= 0 or _audioConfidence <= 0 or _finalScore < 0) then
			set lErrorCode = -2; -- missing parameters
		elseif not exists(select 1 from tbl_quiz_quizQuestionsAnswers where questionID = _questionID and answerID = _recognizedAnswerID) then
			set lErrorCode = -34; -- answer doesn't exist
        end if;
        
        if(lErrorCode = 0) then
			if(_sequenceNumber = -1) then
				select MAX(sequenceNumber) into _sequenceNumber from tbl_usr_userAnswerSpeak;
				set _sequenceNumber = _sequenceNumber + 1;
                
                if(_sequenceNumber is null) then
					set _sequenceNumber = 0;
				end if;
            end if;
            
            -- current user answer exists, first archive it
            if exists(select 1 from tbl_usr_userAnswerSpeak where userID = _userID and questionID = _questionID and quizID = _quizID) then
				insert into tbl_arcv_usr_userAnswerSpeak(sequenceNumber, userID, questionID, quizID, recognizedAnswerID, audioTranscript, audioConfidence, finalScore, answerTime)
				select sequenceNumber, userID, questionID, quizID, recognizedAnswerID, audioTranscript, audioConfidence, finalScore, answerTime
				from tbl_usr_userAnswerSpeak
				where userID = _userID and questionID = _questionID and quizID = _quizID;
				
				delete
				from tbl_usr_userAnswerSpeak
				where userID = _userID and questionID = _questionID and quizID = _quizID;
            end if;
        
			insert into tbl_usr_userAnswerSpeak(sequenceNumber, userID, questionID, quizID, recognizedAnswerID, audioTranscript, audioConfidence, finalScore)
			select _sequenceNumber, _userID, _questionID, _quizID, _recognizedAnswerID, _audioTranscript, _audioConfidence, _finalScore;
            
            call p_usr_recordActivity(_userID, 7, "", 4, _questionID);
            
            if(_isLastItem = 1) then -- this is the last answer in the sequence, calc the quiz summary for the user
				-- current quiz summary exists, first archive it
                if exists(select 1 from tbl_rpt_userQuizSummary where userID = _userID and quizID = _quizID) then
					insert into tbl_arcv_rpt_userQuizSummary(userID, quizID, sequenceNumber, correctAnswersCount, audioConfidenceAvg, finalScoreAvg, reportTime)
                    select userID, quizID, sequenceNumber, correctAnswersCount, audioConfidenceAvg, finalScoreAvg, reportTime
                    from tbl_rpt_userQuizSummary
                    where userID = _userID and quizID = _quizID;
                    
                    delete
                    from tbl_rpt_userQuizSummary
                    where userID = _userID and quizID = _quizID;
                end if;
                
                
                insert into tbl_rpt_userQuizSummary(userID, quizID, sequenceNumber, correctAnswersCount, audioConfidenceAvg, finalScoreAvg)
				select uas.userID, uas.quizID, uas.sequenceNumber, count(qq.questionID), avg(audioConfidence), avg(finalScore)
				from tbl_usr_userAnswerSpeak uas
					left join tbl_quiz_quizQuestions qq
						on uas.quizID = qq.quizID
							and uas.questionID = qq.questionID
							and uas.recognizedAnswerID = qq.correctAnswerID
				where userID = _userID
					and sequenceNumber = _sequenceNumber
          group by uas.userID, uas.quizID, uas.sequenceNumber;
            end if;
			
			select 0 as returnValue, "ok" as errorText;
            select _sequenceNumber as sequenceNumber;
		else
			select lErrorCode as returnValue, "error" as errorText;
		end if;
    else
		select -8 as returnValue, "error" as errorText; -- invalid token
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_updateUserPasswordFromForget` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_updateUserPasswordFromForget`(in _userPhoneNumber varchar(45), 
													  in _userNewPassword varchar(100),
													  in _verificationCode varchar(45))
BEGIN
	declare currentUserID bigint;
    set currentUserID = 0;
    
	if exists (select 1 from tbl_usr_tempVerificationCodes where phoneNumber = _userPhoneNumber
														     and verificationTypeID = 2
                                                             and tempCode = _verificationCode) then -- make sure no one can update the password without the code
		
        set currentUserID = fn_usr_getUserIDByPhoneNumber(_userPhoneNumber);
    
		if (currentUserID > 0) then
			update tbl_usr_users
			set userPassword = _userNewPassword
			where userID = currentUserID;
				
			-- deletes the verification code that was created for this user password replace
			delete from tbl_usr_tempVerificationCodes
			where phonenumber = _userPhoneNumber
				and verificationtypeid = 2;
			
			select 0 as returnValue, "ok" as errorText;
		else
			select -29 as returnValue, "error" as errorText; -- incorrect credentials for forgot password
		end if;
	else
		select -27 as returnValue, "error" as errorText; -- incorrect verification code
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_validateForgotPasswordCredentials` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_validateForgotPasswordCredentials`(in _userPhoneNumber varchar(45))
BEGIN
	declare currentUserID bigint;
    set currentUserID = 0;
    
    set currentUserID = fn_usr_getUserIDByPhoneNumber(_userPhoneNumber);
    
	if (currentUserID > 0) then
		
        select 0 as returnValue, "ok" as errorText;
    else
		select -29 as returnValue, "error" as errorText; -- incorrect credentials for forgot password
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_validateUserToken` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_validateUserToken`(in _userPhoneNumber varchar(45), in _userToken nvarchar(100))
BEGIN
	declare currentUserID bigint;
  set currentUserID = 0;
    
  set currentUserID = fn_usr_getUserIDByPhoneNumber(_userPhoneNumber);
    
	if(fn_usr_validateUserToken(currentUserID, _userToken) > 0) then
			select 0 as returnValue, "ok" as errorText;
      select currentUserID as userID;
  else
  select -8 as returnValue, "error" as errorText;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_usr_validateVerificationCode` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`talkabout_sql`@`%` PROCEDURE `p_usr_validateVerificationCode`(in _userPhoneNumber varchar(45), _verificationCode varchar(45), in _verificationTypeID int)
BEGIN
	if exists (select 1 from tbl_usr_tempVerificationCodes where phoneNumber = _userPhoneNumber 
														     and verificationTypeID = _verificationTypeID
                                                             and tempCode = _verificationCode
                                                             and lastUpdate >= DATE_ADD(current_timestamp, interval -2 minute)) then
		
        select 0 as returnValue, "ok" as errorText;
    else
		select -27 as returnValue, "error" as errorText; -- incorrect verification code
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `view_rpt_userQuizSummary`
--

/*!50001 DROP VIEW IF EXISTS `view_rpt_userQuizSummary`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`talkabout_sql`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_rpt_userQuizSummary` AS select `tbl_rpt_userQuizSummary`.`userID` AS `userID`,`tbl_rpt_userQuizSummary`.`quizID` AS `quizID`,`tbl_rpt_userQuizSummary`.`sequenceNumber` AS `sequenceNumber`,`tbl_rpt_userQuizSummary`.`correctAnswersCount` AS `correctAnswersCount`,`tbl_rpt_userQuizSummary`.`audioConfidenceAvg` AS `audioConfidenceAvg`,`tbl_rpt_userQuizSummary`.`finalScoreAvg` AS `finalScoreAvg`,`tbl_rpt_userQuizSummary`.`reportTime` AS `reportTime` from `tbl_rpt_userQuizSummary` union all select `tbl_arcv_rpt_userQuizSummary`.`userID` AS `userID`,`tbl_arcv_rpt_userQuizSummary`.`quizID` AS `quizID`,`tbl_arcv_rpt_userQuizSummary`.`sequenceNumber` AS `sequenceNumber`,`tbl_arcv_rpt_userQuizSummary`.`correctAnswersCount` AS `correctAnswersCount`,`tbl_arcv_rpt_userQuizSummary`.`audioConfidenceAvg` AS `audioConfidenceAvg`,`tbl_arcv_rpt_userQuizSummary`.`finalScoreAvg` AS `finalScoreAvg`,`tbl_arcv_rpt_userQuizSummary`.`reportTime` AS `reportTime` from `tbl_arcv_rpt_userQuizSummary` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_usr_userAnswerSpeak`
--

/*!50001 DROP VIEW IF EXISTS `view_usr_userAnswerSpeak`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`talkabout_sql`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_usr_userAnswerSpeak` AS select `tbl_usr_userAnswerSpeak`.`sequenceNumber` AS `sequenceNumber`,`tbl_usr_userAnswerSpeak`.`userID` AS `userID`,`tbl_usr_userAnswerSpeak`.`questionID` AS `questionID`,`tbl_usr_userAnswerSpeak`.`quizID` AS `quizID`,`tbl_usr_userAnswerSpeak`.`recognizedAnswerID` AS `recognizedAnswerID`,`tbl_usr_userAnswerSpeak`.`audioTranscript` AS `audioTranscript`,`tbl_usr_userAnswerSpeak`.`audioConfidence` AS `audioConfidence`,`tbl_usr_userAnswerSpeak`.`finalScore` AS `finalScore`,`tbl_usr_userAnswerSpeak`.`answerTime` AS `answerTime` from `tbl_usr_userAnswerSpeak` union all select `tbl_arcv_usr_userAnswerSpeak`.`sequenceNumber` AS `sequenceNumber`,`tbl_arcv_usr_userAnswerSpeak`.`userID` AS `userID`,`tbl_arcv_usr_userAnswerSpeak`.`questionID` AS `questionID`,`tbl_arcv_usr_userAnswerSpeak`.`quizID` AS `quizID`,`tbl_arcv_usr_userAnswerSpeak`.`recognizedAnswerID` AS `recognizedAnswerID`,`tbl_arcv_usr_userAnswerSpeak`.`audioTranscript` AS `audioTranscript`,`tbl_arcv_usr_userAnswerSpeak`.`audioConfidence` AS `audioConfidence`,`tbl_arcv_usr_userAnswerSpeak`.`finalScore` AS `finalScore`,`tbl_arcv_usr_userAnswerSpeak`.`answerTime` AS `answerTime` from `tbl_arcv_usr_userAnswerSpeak` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-23 21:50:08
